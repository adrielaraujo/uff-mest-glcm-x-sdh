from sklearn.neural_network import MLPClassifier as MLP
from sklearn.model_selection import train_test_split
from sklearn import metrics
#from Utils.images import *
from Utils.datasets import separeteClasses, normalizeDataset

class MLPClassifier:

    def __init__(self):

        dirGLCM = "../Datasets/paper1/four-quadrant/brodatz-1/BrodatzImages_GLCM.csv"
        dirSDH = "../Datasets/paper1/four-quadrant/brodatz-1/BrodatzImages_SDH.csv"

        dataset = normalizeDataset(dirSDH)
        X,y = separeteClasses(dataset)


        self.doMLP(X, y)

    def doMLP(self, X, y, X2=None, y2=None):

        print("treinando...")

        if X2 is None or y2 is None:
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1)
        else:
            X_train, X_test, y_train, y_test = X, X2, y, y2

        mlp = MLP(solver="lbfgs", alpha=0.001, hidden_layer_sizes=(4,),random_state=1,
                  learning_rate="constant", learning_rate_init=0.001, max_iter=100,
                  activation="logistic", momentum=0.9, verbose=True, tol=0.0001)

        mlp.fit(X_train, y_train)
        saidas = mlp.predict(X_test)

        print ("---------------------------------------------------")
        print ("saida da rede:\t", saidas)
        print ("saida desesada\t", y_test)
        print ("---------------------------------------------------")

        print ("score: ", (saidas == y_test).sum() / len(X_test))
        print("score:", mlp.score(X_test, y_test))

if __name__ == "__main__":
    program = MLPClassifier()
