from Core.SDH import SumDiffHistogram
from Core.SDH_Unser import UnserSumDiffHistogram
from Core.GLCM import GLCM_Descriptors
from Core.Scikit import ScikitDescriptors
from Utils.images import *
from Utils.MasterThesisImages import *
from Utils.datasets import saveCSV
import matplotlib.pyplot as plt
from skimage import io

class GenerateDatasets:
    def __init__(self):
        #images = brodazImagesToPaper1("../StaticFiles/paper3/ponce/")
        images = ponceImages()
        #features_order = ["asm", "energy", "homogeneity", "correlationS", "entropy", "dissimilarity", "contrast",
        #                  "sum-I", "sum-J", "global_mean", "correlation", "variance", "std-I", "std-J", "cluster_sha"]

        features_order = ["asm", "entropy", 'energy', 'homogeneity', 'correlationS', 'dissimilarity', 'contrast', 'meanI', 'meanJ',
                          'global_mean', 'correlation', 'variance', 'stdJ', 'cluster_shade', 'cluster_prominence']
        distances = [(0,6), (6,6), (6,0), (6,-6)]


        fileGLCM = "paper3/pure-images/synthetic/Synthetic_chevron_GLCM.csv"
        fileOurSDH = "paper3/pure-images/synthetic/Synthetic_chevron_OurSDH.csv"
        fileUnserSDH = "paper3/pure-images/synthetic/Synthetic_chevron_UnserSDH.csv"


        for number, image in enumerate(images):
                print("new image")
                plt.imshow(image, cmap='gray')
                plt.axis('off')
                io.imshow(image, cmap="gray")

                plt.show()
                #for quadrant in splitIntoQuadrants(image):

                for distance in distances:

                    our_dh_features, unser_sdh_features, glcm_features = [], [], []
                    di, dj = distance

                    OurSDH = SumDiffHistogram(image, di, dj, returnJustDifferences=False)
                    UnserSDH = UnserSumDiffHistogram(image, di, dj, returnJustDifferences=False)
                    GLCM = GLCM_Descriptors(image, di, dj, returnJustDifferences=False)

                    our_dh_features += OurSDH.get_features()
                    unser_sdh_features += UnserSDH.get_features()
                    glcm_features += GLCM.get_features()

                    our_dh_features.append(number)
                    unser_sdh_features.append(number)
                    glcm_features.append(number)

                    saveCSV(fileOurSDH, our_dh_features)
                    saveCSV(fileUnserSDH, unser_sdh_features)
                    saveCSV(fileGLCM, glcm_features)
        saveCSV("paper3/pure-images/PonceImages_FeaturesOrder.csv", features_order)
        print("DONE!")


if __name__ == "__main__":
    program = GenerateDatasets()
