from Utils.MasterThesisImages import *
from Utils.images import saveImage, showImage

from Core.SDH import SumDiffHistogram
from Core.SDH_Unser import UnserSumDiffHistogram
from Core.GLCM import GLCM_Descriptors

from Utils.images import ponceImagesToPaper3, syntheticImagesToPaper3

import matplotlib.pyplot as plt
import matplotlib.patheffects as path_effects


class Main():
    def __init__(self):

        #
        #images = ponceImagesToPaper3("StaticFiles/Ponce/paper3/ponce/")
        # images = [np.array([
        #     [0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255],
        #     [255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0],
        #     [0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255],
        #     [255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0],
        #     [0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255],
        #     [255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0],
        #     [0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255],
        #     [255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0],
        #     [0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255],
        #     [255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0],
        #     [0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255],
        #     [255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0],
        #     [0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255],
        #     [255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0],
        #     [0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255],
        #     [255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0],
        #
        # ])]

        # images = [np.array([
        #
        #     [0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255],
        #     [0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255],
        #     [255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0],
        #     [255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0],
        #     [0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255],
        #     [0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255],
        #     [255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0],
        #     [255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0],
        #     [0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255],
        #     [0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255],
        #     [255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0],
        #     [255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0],
        #     [0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255],
        #     [0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255],
        #     [255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0],
        #     [255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0],
        #
        # ])]

        # images = [np.array([
        #     [0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255],
        #     [0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255],
        #     [0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255],
        #     [0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255],
        #     [255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0],
        #     [255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0],
        #     [255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0],
        #     [255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255],
        #     [0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255],
        #     [0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255],
        #     [0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255],
        #     [255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0],
        #     [255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0],
        #     [255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0],
        #     [255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0]
        #
        #
        # ])]

        #images = ponceImages("StaticFiles/Ponce/Dissertação_v2/textures/")
        images = ponceImages("C:\\Users\Aluno\Documents\\UFF\Projetos\[UFF-MEST] GLCM x SDH\StaticFiles\Ponce\Dissertação_v2\\textures\\")
        distance = 6
        angles = np.array(((0,1),(1,-1),  (1,0), (1,1))) * distance #((0,1),(1,-1),  (1,0), (1,1))   # 45 ((1,-1),)

        Our_SDH, Unser_SDH, GLCM_Results, uXg_values, oXg_values  = [], [], [], [], []
        for img_index,image in enumerate(images):
            showImage(image, img_index +40, justText=True)
            #saveImage(image, "StaticFiles/Synthetic Images Dataset/Textures/128x128/texture_" +str(img_index+1) + ".png")
            for angle in angles:
                di, dj = angle
                Our_SDH, Unser_SDH, GLCM_Results = self.get_Results(image, di, dj)

                #lcm = GLCM_Results.glcmImagePlot
                #histograms = Our_SDH.get_histograms()
                # #
                #self.saveSDHPlots(histograms, di, dj, img_index )
                #self.saveGLCM(glcm, di, dj, img_index, add_number=True, revertColor=False)




                uXg_values, oXg_values = self.ourDifferenceValuesIsSmaller(Our_SDH.features, Unser_SDH.features, GLCM_Results.features)

            self.getGlobalDifference(uXg_values, oXg_values)



    def get_Results(self, image, di, dj):

        rjd = True
        Our_SDH, Unser_SDH, GLCM_Results = [], [], []
        GLCM_Results = GLCM_Descriptors(image, di, dj, returnJustDifferences=rjd)
        Our_SDH = SumDiffHistogram(image, di, dj, returnJustDifferences=rjd)
        Unser_SDH = UnserSumDiffHistogram(image, di, dj, returnJustDifferences=rjd)

        return  Our_SDH, Unser_SDH, GLCM_Results

    def ourDifferenceValuesIsSmaller(self, our, unser, glcm):

        oXg_values = []
        uXg_values = []
        oXu_values = []
        oXu_logic = []
        for i in range(len(our)):
            o = our[i]
            u = unser[i]
            g = glcm[i]

            if o > g:
                r_OxG = o - g
            else:
                r_OxG = g - o


            if type(u) is str:
                r_UxG = u
                oXu_logic.append(u)

            else:
                if u > g:
                    r_UxG = u - g
                else:
                    r_UxG = g - u

                oXu_logic.append(r_OxG <= r_UxG)
                if r_UxG > r_OxG: oXu_values.append(r_UxG - r_OxG)
                else: oXu_values.append(r_OxG - r_UxG)

            oXg_values.append(r_OxG)
            uXg_values.append(r_UxG)



        print("U",uXg_values, "\nO", oXg_values,"\nR", oXu_logic)
        #return np.array(uXg_values), np.array(oXg_values)
        return uXg_values, oXg_values

    def getGlobalDifference(self,uXg_values, oXg_values):
        print("\n*******************************************************************\n")
       # uXg_values = uXg_values  # / 4
        #oXg_values = oXg_values  # / 4

        uXg_values = list(uXg_values)
        oXg_values = list(oXg_values)
        u, o = [], []
        for i in range(len(uXg_values)):
            uXg = uXg_values[i]
            oXg = oXg_values[i]
            if type(uXg) is not str and type(uXg) is not np.str_: uXg = ("{:.2f}".format(uXg))
            if type(oXg) is not str and type(oXg) is not np.str_: oXg = ("{:.2f}".format(oXg))

            if uXg == 0: uXg = "No Diff."
            if oXg == 0: oXg = "No Diff."
            u.append(uXg)
            o.append(oXg)

        print("DIFF APROX:\nUxG", u, "\nOxG", o)
        print("\n####################################################################")

    def getAngle(self, di, dj):
        if abs(di) < abs(dj):
            level = "0º"
        elif abs(di) > abs(dj):
            level = "90º"
        elif di == dj and dj > 0:
            level = "135º"
        else:
            level = "45"
        return level

    def saveGLCM(self, glcm, di, dj, img_index, add_number = False, revertColor=False):
        plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)

        if revertColor: cmap = "gray_r"
        else: cmap = "gray"
        #img_index += 24

        fig = plt.imshow(glcm, cmap=cmap)
        plt.axis('off')
        fig.axes.get_xaxis().set_visible(False)
        fig.axes.get_yaxis().set_visible(False)

        if add_number:
            rows, cols = glcm.shape
            for i in range (rows):
                for j in range(cols):
                    #print(i,j)
                    plt.text(j, i,  float(str(glcm[i][j])[:6]) ,va='center', ha='center', size=35)\
                        .set_path_effects([path_effects.Stroke(linewidth=3, foreground='white'), path_effects.Normal()])

        # # io.imshow(image, cmap="gray")
        #plt.show(block=True)
        plt.savefig('StaticFiles/Synthetic Images Dataset/New/GLCMs/D4/GLCM D4 -  Textura ' + str(img_index+1) + ' - ' + self.getAngle(di, dj) + '.png', bbox_inches='tight', pad_inches=-0.1, facecolor="grey", interpolation="nearest")


        #plt.show(block=True)
        plt.close("all")


    def saveSDHPlots(self, histograms, di, dj, img_index):
        if type(histograms) in (list, tuple):
            sum_oc, diff_oc = histograms
            sum, diff, = list(sum_oc.values()), list(diff_oc.values())

            fig, (ax0, ax1) = plt.subplots(nrows=2)

            sumKeys, diffKeys, = list(sum_oc.keys()), list(diff_oc.keys())

            #print("s:", sum_oc, "\nd: ", diff_oc)
            #print("ss:", sum, "\nds: ", diff)


            ################ Preparação
            dicSum, dicDiff = {}, {}
            for i in range (511):
                dicSum[i]= 0

            for i in range (-255,256):
                dicDiff[i] = 0

            for i in sumKeys:
                dicSum[i] = sum_oc[i]

            for i in diffKeys:
                dicDiff[i] = diff_oc[i]

            # get order value
            diffValues = []
            for key in sorted(dicDiff.keys()):
                diffValues.append(dicDiff[key])

            #print("dicSUM", dicSum)
            #print("dicDiff", dicDiff)
            #print (fig, "fi")
            #print("keys", dicDiff.keys())

            #print("values", dicDiff.values())
            #print("vaa", diffValues)

            max_soma =17500
            ax0.tick_params(labelsize=16)
            ax0.set_title("HISTOGRAMA DA SOMA", fontsize= 20)
            ax0.bar(range(len(dicSum.values())), dicSum.values(), align='center', width=1.0)
            ax0.set_ylim([0,max_soma])
            ax0.set_xlim([-10,520])

            ax0.set_yticks([0, 8000, 16000])
            ax0.set_xticks([0, 255, 510])


            ax1.tick_params(labelsize=16)#, axis='x', which='major', pad=15)
            ax1.set_title("HISTOGRAMA DA DIFERENÇA", fontsize= 20)
            ax1.bar(range(len(dicDiff.values())), diffValues, align='center', width=1.0)
            ax1.set_ylim([0,max_soma])
            #ax1.set_xlim([-265,265])
            ax1.set_xlim([-10, 520])

            ax1.set_yticks([0, 8000, 16000])
            #ax1.set_xticks([-255, 0, 255])
            ax1.set_xticks([0, 255, 510])
            ax1.set_xticklabels(['-255','0','255'])

            fig.tight_layout()



            #
            # ax0.bar(range(len(sum)), sum, align='center', width=1.0)
            # #ax0.set_title("HISTOGRAMA DA SOMA")
            # #ax0.set_ylabel("Frequência")
            # plt.setp(ax0.get_xticklabels(), visible=False)
            # ax0.tick_params(axis='both', which='both', length=0)
            #
            # #ax0.set_xticks(np.array(list(range(len(sum)))))
            # #ax0.set_xticklabels(map(int, list(sum_oc.keys())))
            #
            # ax1.bar(range(len(diff)), diff, align='center', width=1.)
            # #
            # #ax1.set_ylabel("Frequência")
            # plt.setp(ax1.get_xticklabels(), visible=False)
            # ax1.tick_params(axis='both', which='both', length=0)
            # #ax1.set_xticks(np.array(list(range(len(diff)))))
            # #ax1.set_xticklabels(map(int, list(diff_oc.keys())))
            #
            # #list = list(range(0, len(diff_oc), len(diff_oc)) )
            # #ax1.set_xticks(np.array(list(range( 0,len(diff_oc),int(len(diff_oc)/3) ))))
            # #ax1.set_xticks(list(range(int(min(diff_oc.keys())), int(max(diff_oc.keys())), int(len(diff_oc.keys())/3))))
            # #ax1.set_xticklabels(map(int, (min(diff_oc.keys()), 0, max(diff_oc.keys()))))
            # print("Size", len(diff_oc))
            # #ax1.set_xticks(np.array([-255, 0, 255]))
            # #ax1.set_xticklabels(map(int, (-255,0,255)))
            # plt.subplots_adjust(hspace=1.5)
            # plt.tight_layout()

















            plt.savefig('StaticFiles/Synthetic Images Dataset/New/SDHs/N-Normalizados/D4/HSD D4 -  Textura ' + str(
                img_index + 1) + ' - ' + self.getAngle(di, dj) + '', bbox_inches='tight', pad_inches=0.0)

            #plt.show(block=True)
            plt.close("all")


if __name__ == "__main__":
    program = Main()

