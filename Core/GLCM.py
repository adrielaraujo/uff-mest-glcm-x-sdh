import numpy as np
from math import sqrt, log10, isnan, pow as mathPow
from skimage.feature import greycomatrix, greycoprops
from timeit import default_timer as timer
from skimage.feature import *
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler

class GLCM_Descriptors:

    def __init__(self,image=None, di= None, dj = None, border = False, returnJustDifferences=False):

        if image is not None and di is not None and dj is not None:
           # grey_level = self.get_gray_levels(image)
           self.distances = (di,dj)
           self.returnJustDifferences = returnJustDifferences
           self.features = self.calc(image, di, dj)


        # if image is not None and di is not None and dj is not None:
        #     #self.calc(image, di, dj, border)
        #     pass
        #
        # self.flagPrepared = False


        # g = greycomatrix(image, [1], [90], levels=256)#, normed = True, )
        # asm = greycoprops(g, 'ASM')[0][0]
        # contrast = greycoprops(g, 'contrast')[0][0]
        # homogeneity = greycoprops(g, 'homogeneity')[0][0]
        # #entropy = greycoprops(g, 'entropy')[0][0]
        # #mean = greycoprops(g, 'mean')[0][0]
        #
        # print("\nGLCM Scikit Image\n>>ASM", asm)
        # #print(">>Entropy", entropy)
        # print(">>Contraste", contrast)
        # print(">>Homogeneity", homogeneity)
        # #print(">>Mean", mean)
        # print("\n---------------------------------------------------------------------------\n")
        #
        # self.features = [asm, entropy, contrast, homogeneity, mean]

        #self.get_features()

    def get_features(self):
        return self.features

    def get_gray_levels(self, image):
        unique = np.unique(image)
        return len(unique)

    def prepare_image(self, image, di, dj, border=False):
        offset = self.get_offset(image, di, dj, border)
        glcm = self.get_glcm(image, offset)
        glcm = self.normalize_glcm(glcm)
        #print("GLCM,\n", glcm)

        self.getGLCMPlotVersion(image, di, dj, )
        #self.glcmImagePlot = glcm

        return glcm

    def get_offset(self, image, di,dj, border=False):


        if dj >=0:
            cutted = image[di:, dj:]
            shape = cutted.shape
            rows = shape[0]
            cols = shape[1]

            if border:
                offset = np.zeros_like(image)
                offset[:rows, :cols] = offset[:rows, :cols] + cutted
            else:
                offset = np.empty((image.shape))
                offset[:] = np.NAN
                offset[:rows, :cols] = cutted

            return offset

        else:
            cutted = image[di:, :image.shape[1]+dj]
            shape = cutted.shape
            rows = shape[0]
            #cols = shape[1]

            if border:
                offset = np.zeros_like(image)
                offset[:rows - di + 1, abs(dj):] = offset[:rows - di + 1, abs(dj):] + cutted
            else:
                offset = np.empty((image.shape))
                offset[:] = np.NAN
                offset[:rows, abs(dj):] = cutted

            return offset

    def get_glcm(self, image, offset):

        grey_levels = self.get_gray_levels(image)
        max_level = int(image.max())+1

        glcm = np.zeros((max_level, max_level))
        glcm = glcm.astype(int)

        #Jeito Não Pythonico
        try:
            rows, cols, canals = image.shape
        except:
            rows, cols = image.shape

        #print("GLCM\n", glcm.shape, "\n", glcm[:4, :4])

        for row in range(rows):
            for col in range(cols):
                value1 = image[row,col]
                value2 = offset[row,col]
                if isnan(value2) == False:
                    glcm[int(round(value1)),int(round(value2))] +=1



        return glcm

    def calc(self, image, di, dj, border=False):

        if abs(di) < abs(dj):
            level = "0º"
        elif abs(di) > abs(dj):
            level = "90º"
        elif di == dj and dj > 0:
            level = "135º"
        else:
            level = "45"
        self.level = level
        self.gray_levels = self.get_gray_levels(image)
        print("\nDi:", di, "  Dj:", dj, "  Level:", level)

        glcm = self.prepare_image(image, di, dj,border)
        #print("GGG", glcm)
        gray_levels = self.get_gray_levels(image)


        #self.showGLCM(glcm)

        #Statitics
        meanI = self.calc_I_MEAN_(glcm,gray_levels)
        meanJ = self.calc_J_MEAN_(glcm,gray_levels)
        std = self.calc_STD(glcm)
        stdI = self.calc_I_STD(glcm,gray_levels)
        stdJ = self.calc_J_STD(glcm, meanJ)

        finalSumMean = meanI + meanJ

        #Descriptors
        asm = self.calc_ASM(glcm)
        energy = self.calc_ENERGY(asm)
        entropy = self.calc_ENTROPY(glcm)
        contrast = self.calc_CONTRAST(glcm)
        dissimilarity = self.calc_DISSIMILARITY(glcm)
        homogeneity = self.calc_HOMOGENEITY(glcm)
        mean = self.calc_MEAN(glcm,gray_levels)
        correlation = self.calc_CORRELATION(glcm, meanI)#self.calc_MEAN(glcm, gray_levels))
        correlationS = self.calc_CORRELATION_SCIKIT(glcm,int(meanI),int(meanJ),gray_levels)
        variance = self.calc_VARIANCE(glcm,meanI)
        cluster_shade = self.calc_CLUSTER_SHADE(glcm,meanI)
        cluster_prominence = self.calc_CLUSTER_PROMINENCE(glcm,meanI)
        global_mean = self.calc_GLOBAL_MEAN(glcm, gray_levels)


        # # #print(glcm)
        # # print("\nGLCM\nASM:", asm)
        # # print("ENERGY", energy)
        # # print("ENTROPY", entropy)
        # # print("CONTRAST:", contrast)
        # # print("DISSIMILARITY", dissimilarity)
        # # print("HOMOGENEITY:", homogeneity)
        # # print("MEAN N:", mean)
        # # print("CORRELATION:", correlation)
        # print("CORRELATION S:", correlationS)
        # # print("VARIANCE:", variance)
        # # print("CLUSTER SHADE:", cluster_shade)
        # # print("CLUSTER PROMINENCE:", cluster_prominence)
        # print("MEAN I", meanI)
        # print ("MEAN J", meanJ)
        # print("GLOBAL MEAN", global_mean)
        # print("STD I", stdI)
        # print ("STD J", stdJ)


        if self.returnJustDifferences:
            #features = [asm,energy,entropy, variance,correlation]#, meanI]
            #features = [entropy, asm, correlation, variance, cluster_shade, cluster_prominence]
            #features = [stdI, stdJ, energy]
            #features = [cluster_shade, cluster_prominence, asm, entropy, variance, correlation, meanI]
            #features = [entropy, variance, correlation, meanI, cluster_shade, cluster_prominence]
            #features = [energy, meanI, correlation, variance, stdJ, cluster_shade, cluster_prominence]
            features = [energy, stdJ, meanI, cluster_shade, cluster_prominence, correlation, variance]
        else:
            #features = [asm, energy, entropy, contrast, dissimilarity, homogeneity, meanI, correlation, variance, cluster_shade, cluster_prominence]
            #features = [asm, energy, homogeneity, entropy, dissimilarity, meanI, contrast, correlation, variance, cluster_shade, cluster_prominence]
            #features = [correlationS, variance, stdI, stdJ, -1]
            #features = [cluster_shade, cluster_prominence, correlation]
            #features = [homogeneity, correlationS, entropy * -1]#, contrast]
            #features = [dissimilarity, contrast, meanI, meanJ, global_mean]
            #features = [cluster_prominence]#/4, cluster_prominence]
            #features = [asm*4, energy]
            #features = [correlation, variance, stdI, stdJ]
            #features = [contrast, meanI, meanJ, global_mean, dissimilarity]
            #features = [cluster_shade, cluster_prominence, homogeneity, contrast, asm, entropy, variance, correlation, meanI, meanJ, global_mean, stdI, stdJ, energy, dissimilarity, correlationS]
            #features = [asm, energy, homogeneity, correlationS, entropy, dissimilarity, contrast, meanI,
            #           meanJ, global_mean, correlation, variance, stdJ, cluster_shade,
            #            cluster_prominence]

            features = [asm, entropy, energy, homogeneity, correlationS, dissimilarity, contrast, meanI, meanJ,
                        global_mean, correlation, variance, stdJ, cluster_shade, cluster_prominence]
            #features = [energy,meanI,correlation, variance, cluster_shade, cluster_prominence]




        features2 = []
        #print(features)
        for i in range(len(features)):
            aux = features[i]
            if type(aux) is not str:
                aux = float('{:.2f}'.format(aux))
            features2.append(aux)

        #print("asm, energy, entropy, contrast, dissimilarity, homogeneity, meanI, correlation, variance, cluster_shade,cluster_prominence")
        #print("cluster_shade, cluster_prominence, homogeneity, contrast, asm, entropy, variance, correlation, meanI, meanJ, global_mean, stdI, stdJ, energy, dissimilarity, correlationS")

        #print("asm, entropy, energy, homogeneity, correlationS, dissimilarity, contrast, meanI, meanJ,global_mean, correlation, variance, stdJ, cluster_shade, cluster_prominence\n")
        #print ("\n")
        #print("\n---------------------------------------------------------------------------\n\n")
        #print(
        #    "[asm, energy, homogeneity, correlationS, entropy, dissimilarity, contrast, meanI, meanJ, global_mean, correlation, variance, stdJ, cluster_shade, cluster_prominence]")

        #features2 = []
        print("G", features2)
        return features2

    def getFatures(self):
        return self.features

    def normalize_glcm(self, glcm):
        glcm = glcm/np.sum(glcm)
        #print("max", glcm.max(), "- ", glcm.min())
        return glcm

    def getGLCMPlotVersion(self, image, di, dj):
        #glcm = np.zeros((255,255))
        #print("grey",self.gray_levels)
        glcm = np.zeros((self.gray_levels, self.gray_levels))
        glcm = glcm.astype(int)

        scaler = MinMaxScaler(feature_range=(0, self.gray_levels-1))
        img = scaler.fit_transform(image)
        img = img.astype(int)

        #img = image
        #img[img == 255] = 1
        offset = self.get_offset(img, di, dj)

        #Jeito Não Pythonico
        try:
            rows, cols, canals = image.shape
        except:
            rows, cols = image.shape

        #print("GLCM\n", glcm.shape, "\n", glcm[:4, :4])

        for row in range(rows):
            for col in range(cols):
                value1 = img[row,col]
                value2 = offset[row,col]
                if isnan(value2) == False:
                    glcm[int(round(value1)),int(round(value2))] +=1



        #print(glcm)

        # plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
        #
        # fig = plt.imshow(glcm, cmap='gray')
        # plt.axis('off')
        # fig.axes.get_xaxis().set_visible(False)
        # fig.axes.get_yaxis().set_visible(False)
        # # # io.imshow(image, cmap="gray")
#        plt.savefig('pict.png', bbox_inches='tight', pad_inches=-1)

        #plt.show(block=True)




        self.glcmImagePlot = self.normalize_glcm(glcm) #glcm
    #

        pass

    # TODA IMPLEMENTAÇÃO DAQUI PARA BAIXO NÃO ESTA SENDO REALIZADA DO JEITO PYTHONICO
    def calc_ASM(self, glcm):
        result = 0
        rows, cols = glcm.shape
        for row in range(rows):
            for col in range(cols):
                #result += glcm[row,col] ** 2
                value = glcm[row,col]
                result += mathPow(value, 2)

        return result

    def calc_ENERGY(self, asm):
        if asm > 0:
            return sqrt(asm)
        else:
            return -1111111111

    def calc_ENTROPY(self, glcm):
        result = 0
        rows, cols = glcm.shape
        for row in range(rows):
            for col in range(cols):
                value = glcm[row,col]
                if value != 0:
                    result += (value) * log10(value)

        return result*-1

    def calc_CONTRAST(self, glcm):
        result = 0
        rows, cols = glcm.shape
        for row in range(rows):
            for col in range(cols):
                value = glcm[row, col]
                result += ((row-col)**2) * value

        return result

    def calc_HOMOGENEITY(self, glcm):
        result = 0
        rows, cols = glcm.shape
        for row in range(rows):
            for col in range(cols):
                value = glcm[row, col]
                result += (1/(1+((row-col)**2))) * value
        return result

    def calc_CORRELATION(self,glcm, mean):

        ###### Equation based on Unser
        result = 0
        rows, cols = glcm.shape
        for row in range(rows):
            for col in range(cols):
                value = glcm[row, col]
                result += (row - mean) * (col-mean) * value
        return result

        ###### Equation Based on Scikit Image
        # std = self.calc_STD(glcm)
        # result = 0
        # rows, cols = glcm.shape
        # for row in range(rows):
        #     for col in range(cols):
        #         value = glcm[row, col]
        #         aux = ((row-mean) * (col-mean) / sqrt(std))
        #         result += value * aux
        # return result

        ###### Equations basead on https://www.hindawi.com/journals/ijbi/2015/267807/
        # x_mean, y_mean, x_std, y_std = 0,0, 0,0
        # rows, cols = glcm.shape
        # for row in range(rows):
        #     for col in range(cols):
        #         value = glcm[row,col]
        #         x_mean += row * value
        #         y_mean += col * value
        #
        # glcm_sum = np.sum(glcm)
        # x_mean = x_mean / glcm_sum
        # y_mean = y_mean / glcm_sum
        #
        # for row in range(rows):
        #     for col in range(cols):
        #         value = glcm[row, col]
        #
        #         x_std += ( mathPow((row - x_mean), 2)) * value
        #         y_std += ( mathPow((col - y_mean), 2)) * value
        #
        # x_std, y_std = sqrt(x_std), sqrt(y_std)
        #
        # result = 0
        # for row in range(rows):
        #     for col in range(cols):
        #         value = glcm[row, col]
        #         result += value * (((row-x_mean) * (col - y_mean)) /  sqrt((x_std**2) * (y_std **2)))
        #
        #
        # return float(result)

    def calc_CORRELATION_SCIKIT(self, glcm, meanI, meanJ, gray_levels):
       # I = np.array(range(gray_levels)).reshape((gray_levels, 1))
        #J = np.array(range(gray_levels)).reshape((1, gray_levels))
        I = np.array(range(glcm.shape[0])).reshape(glcm.shape[0], 1)
        J = np.array(range(glcm.shape[0])).reshape(1, glcm.shape[0])
        iMeanI = np.sum(I - meanI)
        jMeanJ = np.sum(J - meanJ)

        num = (I - meanI) * (J - meanJ)
        sum_num = np.sum(num)
        den = np.sqrt(self.calc_VARIANCE(glcm,meanI) * self.calc_J_STD(glcm,meanJ))#self.calc_I_STD(glcm,gray_levels) * self.calc_J_STD(glcm,gray_levels))
        sum_dem = np.sum(den)
        sum_glcm = np.sum(glcm)
        numDem = np.sum(num / den)
        quo = np.sum(glcm * (num/den))
        numDemAux = 286.673673216

        aux = num / den
        auxSUMFINAL = glcm * (numDemAux)


        ######################## Tradicional
        # result = 0
        # rows, cols = glcm.shape
        # den = np.sqrt(self.calc_I_STD(glcm, gray_levels) * self.calc_J_STD(glcm, gray_levels))
        # for row in range(rows):
        #     for col in range(cols):
        #         value = glcm[row, col]
        #         num = (row - meanI) * (col - meanJ)
        #         result += (num/den) * value



        return quo

    def calc_DISSIMILARITY(self,glcm):

        #### USING PYTHON METHODS
        # dissimilarity = 0
        # for j in range(len(diff_histogram)):
        #     dissimilarity += j * diff_histogram[j]

        #### USING NUMPY METHODS
        # indexes = np.arange(len(diff_histogram))
        # dissimilarity = np.multiply(indexes,diff_histogram)
        # dissimilarity = np.sum(dissimilarity)

        result = 0
        rows, cols = glcm.shape
        for row in range(rows):
            for col in range(cols):
                value = glcm[row, col]
                result += abs(row-col) * value

        return result

    def calc_MEAN(self, glcm, gray_levels):

        sum_values = np.sum(glcm)
        result = sum_values / gray_levels
        return result

    def calc_I_MEAN_(self, glcm, gray_levels):
        #I = np.array(range(gray_levels)).reshape((gray_levels,1))
        I = np.array(range(glcm.shape[0])).reshape(glcm.shape[0], 1)
        result = np.apply_over_axes(np.sum, (I * glcm), axes=(0, 1))[0, 0]
        return result

    def calc_J_MEAN_(self, glcm, gray_levels):
        #J = np.array(range(gray_levels)).reshape((1, gray_levels))
        J = np.array(range(glcm.shape[0])).reshape(1, glcm.shape[0])
        result = np.apply_over_axes(np.sum, (J * glcm), axes=(0, 1))[0, 0]
        return result

    def calc_GLOBAL_MEAN(self, glcm, gray_levels):
        return self.calc_I_MEAN_(glcm,gray_levels) + self.calc_J_MEAN_(glcm,gray_levels)

    def calc_STD(self, glcm):

        result = np.std(glcm)
        return result

    def calc_I_STD(self, glcm, gray_levels):
        #I = np.array(range(gray_levels)).reshape((gray_levels, 1))
        I = np.array(range(glcm.shape[0])).reshape(glcm.shape[0], 1)
        diff_i = np.apply_over_axes(np.sum, (I * glcm), axes=(0, 1))[0, 0] - I
        std_i = np.apply_over_axes(np.sum, (glcm * (diff_i) ** 2), axes=(0, 1))[0, 0]
        return std_i

    def calc_J_STD(self, glcm, mean):
        #J = np.array(range(gray_levels)).reshape((1,gray_levels))
        # J = np.array(range(glcm.shape[0])).reshape(1, glcm.shape[0])
        # diff_j = np.apply_over_axes(np.sum, (J * glcm), axes=(0, 1))[0, 0] - J
        # std_j = np.apply_over_axes(np.sum, (glcm * (diff_j) ** 2), axes=(0, 1))[0, 0]
        # return std_j
        result = 0
        rows, cols = glcm.shape
        for row in range(rows):
            for col in range(cols):
                value = glcm[row, col]
                result +=  ((col - mean)**2) * value

        return  result

    def calc_VARIANCE(self, glcm, mean):
        result = 0
        rows, cols = glcm.shape
        for row in range(rows):
            for col in range(cols):
                value = glcm[row, col]
                result +=  ((row - mean)**2) * value

        return result

    def calc_CLUSTER_SHADE(self, glcm, mean):
        result = 0
        rows, cols = glcm.shape
        for row in range(rows):
            for col in range(cols):
                value = glcm[row, col]
                result += ((row + col - (2 * mean))**3) * value

        return result

    def calc_CLUSTER_PROMINENCE(self, glcm, mean):
        result = 0
        rows, cols = glcm.shape
        for row in range(rows):
            for col in range(cols):
                value = glcm[row, col]
                result += ((row + col - (2 * mean)) ** 4) * value

        return result