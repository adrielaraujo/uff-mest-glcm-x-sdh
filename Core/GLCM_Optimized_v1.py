import numpy as np
from math import sqrt, log10, isnan, pow as mathPow
from skimage.feature import greycomatrix, greycoprops
from timeit import default_timer as timer
from skimage.feature import *

from skimage.data import astronaut
from skimage.color import rgb2grey
from sklearn.preprocessing import MinMaxScaler

class GLCM_Descriptors:

    def __init__(self,image=None, di= None, dj = None, border = False):

        if image is not None and di is not None and dj is not None:
           # grey_level = self.get_gray_levels(image)

            self.calc(image, di, dj)


        # if image is not None and di is not None and dj is not None:
        #     #self.calc(image, di, dj, border)
        #     pass
        #
        # self.flagPrepared = False


        # g = greycomatrix(image, [1], [90], levels=256)#, normed = True, )
        # asm = greycoprops(g, 'ASM')[0][0]
        # contrast = greycoprops(g, 'contrast')[0][0]
        # homogeneity = greycoprops(g, 'homogeneity')[0][0]
        # #entropy = greycoprops(g, 'entropy')[0][0]
        # #mean = greycoprops(g, 'mean')[0][0]
        #
        # print("\nGLCM Scikit Image\n>>ASM", asm)
        # #print(">>Entropy", entropy)
        # print(">>Contraste", contrast)
        # print(">>Homogeneity", homogeneity)
        # #print(">>Mean", mean)
        # print("\n---------------------------------------------------------------------------\n")
        #
        # self.features = [asm, entropy, contrast, homogeneity, mean]

        #self.get_features()

    def get_features(self):
        return self.features

    def get_gray_levels(self, image):
        unique = np.unique(image)
        return len(unique)

    def prepare_image(self, image, di, dj, border=False):
        offset = self.get_offset(image, di, dj, border)
        glcm = self.get_glcm(image, offset)
        glcm = self.normalize_glcm(glcm)

        return glcm

    def get_offset(self, image, di,dj, border=False):


        if dj >=0:
            cutted = image[di:, dj:]
            shape = cutted.shape
            rows = shape[0]
            cols = shape[1]

            if border:
                offset = np.zeros_like(image)
                offset[:rows, :cols] = offset[:rows, :cols] + cutted
            else:
                offset = np.empty((image.shape))
                offset[:] = np.NAN
                offset[:rows, :cols] = cutted

            return offset

        else:
            cutted = image[di:, :image.shape[1]+dj]
            shape = cutted.shape
            rows = shape[0]
            #cols = shape[1]

            if border:
                offset = np.zeros_like(image)
                offset[:rows - di + 1, abs(dj):] = offset[:rows - di + 1, abs(dj):] + cutted
            else:
                offset = np.empty((image.shape))
                offset[:] = np.NAN
                offset[:rows, abs(dj):] = cutted

            return offset

    def get_glcm(self, image, offset):

        max_level = int(image.max())+1

        glcm = np.zeros((max_level, max_level))
        glcm = glcm.astype(int)

        rows, cols = image.shape

        #print("GLCM\n", glcm.shape, "\n", glcm[:4, :4])

        for row in range(rows):
            for col in range(cols):
                value1 = image[row,col]
                value2 = offset[row,col]
                if isnan(value2) == False:
                    glcm[int(round(value1)),int(round(value2))] +=1

        return glcm

    def calc(self, image, di, dj, border=False):

        glcm = self.prepare_image(image, di, dj,border)
        gray_levels = self.get_gray_levels(image)

        asm = self.calc_ASM(glcm)
        energy = self.calc_ENERGY(asm)
        entropy = self.calc_ENTROPY(glcm)
        start = timer()
        contrast = self.calc_CONTRAST(glcm)
        end = timer()
        dissimilarity = self.calc_DISSIMILARITY(glcm)
        homogeneity = self.calc_HOMOGENEITY(glcm)
        mean = self.calc_MEAN(glcm,gray_levels)
        correlation = self.calc_CORRELATION(glcm, mean)
        variance = self.calc_VARIANCE(glcm,mean)
        cluster_shade = self.calc_CLUSTER_SHADE(glcm,mean)
        cluster_prominence = self.calc_CLUSTER_PROMINENCE(glcm,mean)
       # std = self.calc_STD(glcm)

        print("\nGLCM\nASM:", asm)
        print("ENERGY", energy)
        print("ENTROPY", entropy)
        print("CONTRAST:", contrast)
        print("DISSIMILARITY", dissimilarity)
        print("HOMOGENEITY:", homogeneity)
        print("MEAN:", mean)
        print("CORRELATION:", correlation)
        print("VARIANCE:", variance)
        print("CLUSTER SHADE:", cluster_shade)
        print("CLUSTER PROMINENCE:", cluster_prominence)
       # print("STD", std)

        print("Time using python methods: ", end - start)

        print("\n---------------------------------------------------------------------------\n\n")

    def normalize_glcm(self, glcm):
        glcm = glcm/np.sum(glcm)
        return glcm



    def calc_ASM(self, glcm):
        return np.sum(np.power(glcm, 2))

    def calc_ENERGY(self, asm):
        return sqrt(asm)

    def calc_ENTROPY(self, glcm):
        aux = glcm
        aux[aux == 0] = 1
        return np.nansum(np.log10(aux) * -aux)

    def calc_CONTRAST(self, glcm):
        rows, cols = glcm.shape
        i,j = np.where(glcm == glcm)
        aux = np.power(i-j, 2).reshape((rows, cols))
        return np.nansum(glcm * aux)

    def calc_HOMOGENEITY(self, glcm):
        rows, cols = glcm.shape
        i, j = np.where(glcm == glcm)
        aux = np.power(i - j, 2).reshape((rows, cols))
        aux = 1 / (1 + aux)
        return np.nansum(glcm * aux)

    def calc_CORRELATION(self,glcm, mean):
        ###### Equation based on Unser
        rows, cols = glcm.shape
        i, j = np.where(glcm == glcm)
        i = (i - mean).reshape((rows, cols))
        j = (j - mean).reshape((rows, cols))
        return np.nansum(i * j * glcm)

    def calc_DISSIMILARITY(self,glcm):
        rows, cols = glcm.shape
        i, j = np.where(glcm == glcm)
        aux = (i - j).reshape((rows, cols))
        return np.nansum(np.absolute(aux) * glcm)

    def calc_MEAN(self, glcm, gray_levels):
        sum_values = np.sum(glcm)
        result = sum_values / gray_levels
        return result

    def calc_STD(self, glcm):
        result = np.std(glcm)
        return result

    def calc_VARIANCE(self, glcm, mean):
        rows, cols = glcm.shape
        i, j = np.where(glcm == glcm)
        aux = np.power(i - mean,2).reshape((rows, cols))
        return np.nansum(aux * glcm)

    def calc_CLUSTER_SHADE(self, glcm, mean):
        rows, cols = glcm.shape
        i, j = np.where(glcm == glcm)
        aux = np.power(i + j - (2 * mean), 3).reshape((rows, cols))
        return np.nansum(aux * glcm)

    def calc_CLUSTER_PROMINENCE(self, glcm, mean):
        rows, cols = glcm.shape
        i, j = np.where(glcm == glcm)
        aux = np.power(i + j - (2 * mean), 4).reshape((rows, cols))
        return np.nansum(aux * glcm)


if __name__ == "__main__":
    program = GLCM_Descriptors()

    image1 = np.array([(0, 0, 0, 0, 0, 0, 0, 0),
                       (0, 0, 0, 0, 0, 0, 0, 0),
                       (1, 1, 1, 1, 1, 1, 1, 1),
                       (1, 1, 1, 1, 1, 1, 1, 1),
                       (2, 2, 2, 2, 2, 2, 2, 2),
                       (2, 2, 2, 2, 2, 2, 2, 2),
                       (3, 3, 3, 3, 3, 3, 3, 3),
                       (3, 3, 3, 3, 3, 3, 3, 3)])

    image = np.array([(0, 0, 0, 1, 1, 0, 0, 0),
              (0, 0, 0, 0, 0, 0, 0, 0),
              (1, 1, 1, 1, 3, 3, 1, 1),
              (1, 1, 2, 2, 1, 1, 1, 1),
              (2, 2, 0, 0, 2, 2, 2, 2),
              (3, 3, 2, 2, 2, 2, 2, 2),
              (3, 3, 3, 3, 1, 1, 3, 3),
              (2, 2, 3, 3, 3, 3, 3, 3)])

    image3 = np.array([[1,2,3],[1,3,2]])

    scaler = MinMaxScaler(feature_range=(0, 255))
    imageX = rgb2grey(astronaut())
    imageX = scaler.fit_transform(imageX)
    imageX = imageX.astype(int)


    glcm = program.prepare_image(imageX, 0, 1)
    mean = program.calc_MEAN(glcm, program.get_gray_levels(imageX))


    program.calc_VARIANCE(glcm,mean)
