from skimage.feature import greycoprops, greycomatrix
from Utils.images import convertToAngle
import numpy as np

class ScikitDescriptors:

    def __init__(self, image, di, dj):
        angle = convertToAngle(di, dj)
        self.features = self.calc(image, angle)

    def get_gray_levels(self, image):
        unique = np.unique(image)
        return len(unique)

    def calc(self, image, angle):
        levels = self.get_gray_levels(image)
        #print("-----",levels)

        g = greycomatrix(image, [2], [angle], levels=256, normed=True)
        #print("G sci\n", g)
        ASM = greycoprops(g, 'ASM')[0][0]
        energy = greycoprops(g, 'energy')[0][0]
        contrast = greycoprops(g, 'contrast')[0][0]
        dissimilarity = greycoprops(g, 'dissimilarity')[0][0]
        homogeneity = greycoprops(g, 'homogeneity')[0][0]
        correlation = greycoprops(g, 'correlation')[0][0]

        ############################
        # num_level = 2
        # P = g
        # results = np.zeros((1, 1), dtype=np.float64)
        # I = np.array(range(num_level)).reshape((num_level, 1, 1, 1))
        # J = np.array(range(num_level)).reshape((1, num_level, 1, 1))
        # diff_i = I - np.apply_over_axes(np.sum, (I * P), axes=(0, 1))[0, 0]
        # diff_j = J - np.apply_over_axes(np.sum, (J * P), axes=(0, 1))[0, 0]
        #
        # std_i = np.sqrt(np.apply_over_axes(np.sum, (P * (diff_i) ** 2),
        #                                    axes=(0, 1))[0, 0])
        # std_j = np.sqrt(np.apply_over_axes(np.sum, (P * (diff_j) ** 2),
        #                                    axes=(0, 1))[0, 0])
        # cov = np.apply_over_axes(np.sum, (P * (diff_i * diff_j)),
        #                          axes=(0, 1))[0, 0]
        #
        #
        # mediaI =  np.apply_over_axes(np.sum, (I * P), axes=(0, 1))[0, 0]
        # # handle the special case of standard deviations near zero
        # mask_0 = std_i < 1e-15
        # mask_0[std_j < 1e-15] = True
        # results[mask_0] = 1
        #
        # # handle the standard case
        # mask_1 = mask_0 == False
        # cor2 = cov[mask_1] / (std_i[mask_1] * std_j[mask_1])
        # aux = cov[mask_1]

        ############################

        # print("\n\nGLCM\nASM", ASM,  "\nEnergy", energy, "\nContrast", contrast, "\nDissimilarity", dissimilarity,
        #       "\nHomogeneity", homogeneity, "\nCorrelation", correlation)

        features = [ASM, energy, contrast,dissimilarity,homogeneity,correlation]
        features2 = []
        for i in range(len(features)):
            aux = features[i]
            if type(aux) is not str:
                aux = float('{:.5f}'.format(aux))
            features2.append(aux)

        print ("\nS", features2)

        return features