#from skimage.data import binary_blobs, astronaut
#from skimage.io import imread, imshow, imsave
#from skimage.viewer import ImageViewer
#from skimage.feature import greycoprops
#import matplotlib.pyplot as plt
import numpy as np
from math import sqrt, log10,pow as mathPow
#from scipy.ndimage import imread
from timeit import default_timer as timer

from skimage.data import astronaut
from skimage.color import rgb2grey
from sklearn.preprocessing import MinMaxScaler

class SumDiffHistogram:

    def __init__(self,image=None, di= None, dj = None, border = False):
        if image is not None and di is not None and dj is not None:
            self.features = self.calc(image, di, dj, border)
        self.flagPrepared = False

        #self.get_features()

    def get_features(self):
        return self.features

    def prepare_image(self, image, di, dj, border=False):

        offset = self.get_offset(image, di, dj, border)

        sum = image + offset
        diff = image - offset

        sum = sum[~np.isnan(sum)]
        diff = diff[~np.isnan(diff)]

        sum_occurences = self.count_occurences(sum)
        diff_occurences = self.count_occurences(diff)

        sum_occurences[1] = self.normalize_histogram(sum_occurences[1])
        diff_occurences[1] = self.normalize_histogram(diff_occurences[1])

        # Não utilizado nesta versão
        #sum_vector = np.sort(np.reshape(sum, sum.size))
        #diff_vector = np.sort(np.reshape(diff, sum.size))

        #print("Original:\n", image)
        #print("\nOffset:\n", offset)
        #print("\nSum:\n", sum, "\nDiff:\n", diff)
        #print("\nOccurences:\nSum:", sum_occurences, "\nDiff:", diff_occurences)
        #print("\nVectors:\nSum:", len(sum_vector), sum_vector, "\nDiff:", len(diff_vector), diff_vector)


        self.flagPrepared = True

        return sum_occurences, diff_occurences


    def calc(self, image, di, dj, border=False):

        sum_occurences, diff_occurences = self.prepare_image(image,di,dj, border)

        if abs(di) < abs(dj): level = "0º"
        elif abs(di) > abs(dj): level = "90º"
        elif di == dj and dj>0: level = "45º"
        else: level = "135"
        print("Di:", di, "  Dj:", dj, "  Level:", level)


        asm = self.calc_ASM(sum_occurences) #self.calc_Total_ASM(sum_occurences,diff_occurences)#
        energy = self.calc_ENERGY(asm)
        entropy = self.calc_ENTROPY(sum_occurences)
        start = timer()
        contrast = self.calc_CONTRAST(diff_occurences)
        end = timer()
        dissimilarity = self.calc_DISSIMILARITY(diff_occurences)
        homogeneity = self.calc_HOMOGENEITY(diff_occurences)
        mean = self.calc_MEAN(sum_occurences, self.get_gray_levels(image))
        correlation = self.calc_CORRELATION(sum_occurences,diff_occurences,mean)
        print("Time using python methods: ", end - start)
        variance = self.calc_VARIANCE(sum_occurences,diff_occurences,mean)
        cluster_shade = self.calc_CLUSTER_SHADE(sum_occurences,mean)
        cluster_prominence = self.calc_CLUSTER_PROMINENCE(sum_occurences,mean)
       # std = self.calc_STD(sum_occurences, diff_occurences)



        print("\nSHD\nASM:", asm)
        print("ENERGY", energy)
        print("ENTROPY", entropy)
        print("CONTRAST:", contrast)
        print("DISSIMILARITY", dissimilarity)
        print("HOMOGENEITY:", homogeneity)
        print("MEAN:", mean)
        print("CORRELATION:", correlation)
        print("VARIANCE:", variance)
        print("CLUSTER SHADE:", cluster_shade)
        print("CLUSTER PROMINENCE:", cluster_prominence)
        #print("STD:", std)
        #print("\n---------------------------------------------------------------------------\n\n")


        #features = [asm, energy, contrast, homogeneity, correlation, dissimilarity]

        # correlation = 0.5*(result_sum - result_diff)

        features = [asm, entropy, contrast, homogeneity, mean]


        return features

    def normalize_histogram(self, histogram):

        #sum_values = sum(histogram.values())
        sum_values = np.nansum(histogram)
        normalized_histogram = histogram / sum_values # {k: v/sum_values for k, v in histogram.items()}
        return normalized_histogram

    def get_offset(self, image, di,dj, border=False):


        if dj >=0:
            cutted = image[di:, dj:]
            shape = cutted.shape
            rows = shape[0]
            cols = shape[1]

            if border:
                offset = np.zeros_like(image)
                offset[:rows, :cols] = offset[:rows, :cols] + cutted
            else:
                offset = np.empty((image.shape))
                offset[:] = np.NAN
                offset[:rows, :cols] = cutted

            return offset

        else:
            cutted = image[di:, :image.shape[1]+dj]
            shape = cutted.shape
            rows = shape[0]
            cols = shape[1]

            if border:
                offset = np.zeros_like(image)
                offset[:rows - di + 1, abs(dj):] = offset[:rows - di + 1, abs(dj):] + cutted
            else:
                offset = np.empty((image.shape))
                offset[:] = np.NAN
                offset[:rows, abs(dj):] = cutted

            return offset


    def count_occurences(self,image):
        unique, counts = np.unique(image, return_counts=True)
        return [unique, counts]
        #return dict(zip(unique, counts))

    def get_gray_levels(self, image):
        unique = np.unique(image)
        return len(unique)

    def get_histogram(self, image, title):
        N = len(image)
        plt.bar(range(N), image.values(), align='center')#, color=jet(np.linspace(0, 1.0, N)))
        plt.xticks(range(N), image.k())
        plt.title(title)
        plt.ylabel("Frequency")
        plt.xlabel("Value")
        plt.show()

    def calc_ASM(self, sum_histogram):
        return np.sum(np.power(sum_histogram[1], 2))

    def calc_Diff_ASM(self, diff_histogram):

        result = 0

        #frequences = list(diff_histogram.values())

        #if len(set(frequences)) == 1:
        for key, value in diff_histogram.items():
            result += value**2
        return result

    def calc_Total_ASM(self, sum_histogram, diff_histogram):

        sum = self.calc_ASM(sum_histogram)
        diff = self.calc_Diff_ASM(diff_histogram)

        return sum + diff

    def calc_ENERGY(self, asm):
        return sqrt(asm)

    def calc_CONTRAST(self, diff_histogram):
        indexes, values = diff_histogram
        return np.nansum(np.power(indexes, 2) * values)

    def calc_HOMOGENEITY(self, diff_histogram):
        indexes, values = diff_histogram
        return np.nansum((1 / (1 + np.power(indexes, 2))) * values)

    def calc_CORRELATION(self,sum_histogram, diff_histogram, mean):

        ###### Equations based on Unser
        indexes, values = sum_histogram
        result_sum = np.nansum(np.power(indexes - (2 * mean),2) * values)

        indexes, values = diff_histogram
        result_diff = np.nansum(np.power(indexes, 2) * values)

        result = 0.25 * (result_sum - result_diff)

        return result

    def calc_DISSIMILARITY(self,diff_histogram):
        return np.nansum(np.absolute(diff_histogram[0]) * diff_histogram[1])

    def calc_MEAN(self, sum_histogram, gray_levels):
        sum_values = np.nansum(sum_histogram[1])
        result = sum_values/gray_levels
        return result

    def calc_STD(self, sum_histogram, diff_histogram):

        sum_std = np.std(np.array(list(sum_histogram[1])))
        diff_std = np.std(np.array(list(diff_histogram[1])))

        return [sum_std, diff_std]

    def calc_ENTROPY(self, sum_histogram):
        values = sum_histogram[1]
        values[values == 0] = 1
        return np.nansum(np.log10(values) * -values)

    def calc_VARIANCE(self,sum_histogram, diff_histogram, mean):
        '''Na proposta de Unser(86) o resultado é divido por 2. Aqui dividimos por 4'''
        indexes, values = sum_histogram
        result_sum = np.nansum(np.power(indexes - (2 * mean), 2) * values)

        indexes, values = diff_histogram
        result_diff = np.nansum(np.power(indexes, 2) * values)

        result = 0.25 * (result_sum + result_diff)

        return result

    def calc_CLUSTER_SHADE(self, sum_histogram, mean):
        indexes, values = sum_histogram
        return np.nansum(np.power(indexes - (2 * mean), 3) * values)

    def calc_CLUSTER_PROMINENCE(self, sum_histogram, mean):
        indexes, values = sum_histogram
        return np.nansum(np.power(indexes - (2 * mean), 4) * values)



if __name__ == '__main__':
    scaler = MinMaxScaler(feature_range=(0, 255))
    imageX = rgb2grey(astronaut())
    imageX = scaler.fit_transform(imageX)
    imageX = imageX.astype(int)

    main = SumDiffHistogram()
    sum_occurences, diff_occurences = main.prepare_image(imageX,0,1)

    mean = main.calc_MEAN(sum_occurences,main.get_gray_levels(imageX))
    x = main.calc_CLUSTER_SHADE(sum_occurences,mean)
    y = main.calc_CLUSTER_PROMINENCE(sum_occurences,mean)
    print(x, "\n", y)