#from skimage.data import binary_blobs, astronaut
#from skimage.io import imread, imshow, imsave
#from skimage.viewer import ImageViewer
#from skimage.feature import greycoprops
#import matplotlib.pyplot as plt
import numpy as np
from math import sqrt, log10,pow as mathPow
#from scipy.ndimage import imread
from timeit import default_timer as timer

from builtins import sum
from skimage.data import astronaut
from skimage.color import rgb2grey
from sklearn.preprocessing import MinMaxScaler

class UnserSumDiffHistogram:

    def __init__(self,image=None, di= None, dj = None, border = False, comparingScikit = False, returnJustDifferences=False):
        if image is not None and di is not None and dj is not None:
            self.comparingScikit = comparingScikit
            self.returnJustDifferences = returnJustDifferences
            self.features = self.calc(image, di, dj, border)
        self.flagPrepared = False

        #self.get_features()

    def get_features(self):
        return self.features

    def prepare_image(self, image, di, dj, border=False):

        offset = self.get_offset(image, di, dj, border)

        sum = image + offset
        diff = image - offset

        sum = sum[~np.isnan(sum)]
        diff = diff[~np.isnan(diff)]

        sum_occurences = self.count_occurences(sum)
        diff_occurences = self.count_occurences(diff)

        sum_occurences = self.normalize_histogram(sum_occurences)
        diff_occurences = self.normalize_histogram(diff_occurences)

        # Não utilizado nesta versão
        #sum_vector = np.sort(np.reshape(sum, sum.size))
        #diff_vector = np.sort(np.reshape(diff, sum.size))

        #print("Original:\n", image)
        #print("\nOffset:\n", offset)
        #print("\nSum:\n", sum, "\nDiff:\n", diff)
        #print("\nUnser:\nSum:", sum_occurences, "\nDiff:", diff_occurences)
        #print("\nVectors:\nSum:", len(sum_vector), sum_vector, "\nDiff:", len(diff_vector), diff_vector)


        self.flagPrepared = True

        return sum_occurences, diff_occurences


    def calc(self, image, di, dj, border=False):

        sum_occurences, diff_occurences = self.prepare_image(image,di,dj, border)

        if abs(di) < abs(dj): level = "0º"
        elif abs(di) > abs(dj): level = "90º"
        elif di == dj and dj>0: level = "45º"
        else: level = "135"
        self.level = level
        #print("\nDi:", di, "  Dj:", dj, "  Level:", level)

        # Statitics
        meanI = self.calc_I_MEAN_(sum_occurences, self.get_gray_levels(image))
        meanJ = self.calc_I_MEAN_(diff_occurences, self.get_gray_levels(image))
        stdI = self.calc_SUM_STD(sum_occurences, self.get_gray_levels(image))
        stdJ = self.calc_SUM_STD(diff_occurences, self.get_gray_levels(image))
        std = self.calc_STD(sum_occurences, diff_occurences)

        # Descriptors
        asm = self.calc_ASM(sum_occurences, diff_occurences) #self.calc_Total_ASM(sum_occurences,diff_occurences)#
        energy = self.calc_ENERGY(asm)
        entropy = self.calc_ENTROPY(sum_occurences, diff_occurences)
        contrast = self.calc_CONTRAST(diff_occurences)
        dissimilarity = self.calc_DISSIMILARITY(diff_occurences)
        homogeneity = self.calc_HOMOGENEITY(diff_occurences)
        mean = self.calc_MEAN(sum_occurences, self.get_gray_levels(image))
        meanS = self.calc_I_MEAN_(sum_occurences, self.get_gray_levels(image))
        correlation = self.calc_CORRELATION(sum_occurences,diff_occurences,meanI)
        #correlationS = self.calc_CORRELATION_SCIKIT(sum_occurences, diff_occurences, int(meanS), int(meanJ),
       #                                             self.get_gray_levels(image))
        variance = self.calc_VARIANCE(sum_occurences,diff_occurences,meanI)
        cluster_shade = self.calc_CLUSTER_SHADE(sum_occurences,meanI)
        cluster_prominence = self.calc_CLUSTER_PROMINENCE(sum_occurences,meanI)



        # # print("\nSHD\nASM:", asm)
        # # print("ENERGY", energy)
        # # print("ENTROPY", entropy)
        # # print("CONTRAST:", contrast)
        # # print("DISSIMILARITY", dissimilarity)
        # # print("HOMOGENEITY:", homogeneity)
        # print("MEAN N:", mean)
        # # print("CORRELATION:", correlation)
        # print("CORRELATION S:", correlationS)
        # # print("VARIANCE:", variance)
        # # print("CLUSTER SHADE:", cluster_shade)
        # # print("CLUSTER PROMINENCE:", cluster_prominence)
        # print("MEAN I", meanI)
        # print("MEAN J", meanJ)
        # print("STD I", stdI)
        # print("STD J", stdJ)
        # # print("STD:", std)
        #print("\n---------------------------------------------------------------------------\n\n")

        #correlation = correlationS
        #print("SH\n", sum_occurences, "\nDH\n", diff_occurences)



        if self.comparingScikit:
            features =  [asm, energy, contrast, dissimilarity, homogeneity, correlation]

        elif self.returnJustDifferences:
            #features = [entropy, asm, correlation, variance, cluster_shade, cluster_prominence]#, meanI]
            #features = [-1, -1, -1]
            #features = [entropy, variance, correlation, meanI, cluster_shade, cluster_prominence]
            #features = [cluster_shade, cluster_prominence, asm, entropy, variance, correlation, meanI]
            #features = [energy, meanI, correlation, variance, stdJ, cluster_shade, cluster_prominence]
            features = [0, 0, meanI, cluster_shade, cluster_prominence, correlation, variance]
        else:
            #features = [asm, energy, entropy, contrast, dissimilarity, homogeneity, finalSumMean, correlation, variance, cluster_shade, cluster_prominence]
            #features = [asm, energy, homogeneity, entropy, dissimilarity, meanI, contrast, correlation, variance, cluster_shade, cluster_prominence]
            #features = [-1, variance, -1, -1 ,-1]
            #features = [cluster_shade, cluster_prominence, correlation]
            #features = [homogeneity, 0, entropy]#,  entropy * -1, correlationS]
            #features = [0, contrast, meanI, 0, 0]
            #features = [cluster_shade*30]#/4, cluster_prominence]
            #features = [asm*4,0]#energy]
            #features = [correlation, variance, 0, 0]
            #features = [cluster_shade, cluster_prominence, homogeneity, contrast, asm, entropy, variance, correlation, meanI, -1,-1-1,-1,-1,-1,-1,-1]
            #features = [asm, 'N/A', homogeneity, 'N/A', entropy, 'N/A', contrast, meanI,
            #            'N/A','N/A', correlation, variance, 'N/A', cluster_shade,
            #            cluster_prominence]
            features = [asm, entropy, 'N/A', homogeneity, 'N/A', 'N/A', contrast, meanI, 'N/A',
                        'N/A', correlation, variance, 'N/A', cluster_shade, cluster_prominence]  #DISSERTATIOM ORDER

            #features = [asm, entropy, 0, homogeneity, 0, 0, contrast, meanI, -99999,
            #            -99999, correlation, variance, 0, cluster_shade, cluster_prominence]

            #features = [energy,meanI,correlation, variance, cluster_shade, cluster_prominence]

            #features = [asm, -99999, homogeneity, -99999, entropy, -99999, contrast, meanI,
             #           -99999, -99999, correlation, variance, -99999, -99999, cluster_shade,
              #          cluster_prominence]


        features2 = []
        for i in range(len(features)):
            aux = features[i]
            if type(aux) is not str:
                aux = float('{:.2f}'.format(aux))
            features2.append(aux)
        #print(
        #    "asm, energy, entropy, contrast, dissimilarity, homogeneity, meanI, correlation, variance, cluster_shade,cluster_prominence")
        #print("asm,energy,entropy, variance,correlation, correlationS")
        ##print("cluster_shade, cluster_prominence, homogeneity, contrast, asm, entropy, variance, correlation, meanI, meanJ, global_mean, stdI, stdJ, energy, dissimilarity, correlationS")

        print("U",features2)

        return  features2
    def getFatures(self):
        return self.features

    def normalize_histogram(self, histogram):

        sum_values = sum(histogram.values())

        normalized_histogram = {k: v/sum_values for k, v in histogram.items()}
        return  normalized_histogram




    def get_offset(self, image, di,dj, border=False):


        if dj >=0:
            cutted = image[di:, dj:]
            shape = cutted.shape
            rows = shape[0]
            cols = shape[1]

            if border:
                offset = np.zeros_like(image)
                offset[:rows, :cols] = offset[:rows, :cols] + cutted
            else:
                offset = np.empty((image.shape))
                offset[:] = np.NAN
                offset[:rows, :cols] = cutted

            return offset

        else:
            cutted = image[di:, :image.shape[1]+dj]
            shape = cutted.shape
            rows = shape[0]
            cols = shape[1]

            if border:
                offset = np.zeros_like(image)
                offset[:rows - di + 1, abs(dj):] = offset[:rows - di + 1, abs(dj):] + cutted
            else:
                offset = np.empty((image.shape))
                offset[:] = np.NAN
                offset[:rows, abs(dj):] = cutted

            return offset


    def count_occurences(self,image):
        unique, counts = np.unique(image, return_counts=True)
        return dict(zip(unique, counts))

    def get_gray_levels(self, image):
        unique = np.unique(image)
        return len(unique)

    def get_histogram(self, image, title):
        N = len(image)
        plt.bar(range(N), image.values(), align='center')#, color=jet(np.linspace(0, 1.0, N)))
        plt.xticks(range(N), image.k())
        plt.title(title)
        plt.ylabel("Frequency")
        plt.xlabel("Value")
        plt.show()



    # VALIDAR IMPLEMENTACAO DE EQUACOES APRESENTADAS POR ACORDO COM UNSER,86 (PAPER NO REPOSITORIO)

    def calc_ASM(self, sum_histogram, diff_histogram):

        result, result_sum, result_diff = 0,0,0

        #frequences = list(sum_histogram.values())

        #if len(set(frequences)) == 1:
        for key, value in sum_histogram.items():
            result_sum += value ** 2

        for key, value in diff_histogram.items():
            result_diff += value ** 2

        return result_sum * result_diff

    def calc_Diff_ASM(self, diff_histogram):

        result = 0

        #frequences = list(diff_histogram.values())

        #if len(set(frequences)) == 1:
        for key, value in diff_histogram.items():
            result += value**2
        return result

    def calc_Total_ASM(self, sum_histogram, diff_histogram):

        sum = self.calc_ASM(sum_histogram)
        diff = self.calc_Diff_ASM(diff_histogram)

        return sum + diff

    def calc_ENERGY(self, asm):
        if asm > 0:
            return sqrt(asm)
        else:
            return -1111111111

    def calc_CONTRAST(self, diff_histogram):

        #### USING PYTHON METHODS
        # result = 0
        # for j in range(len(diff_histogram)):
        #     result += (j**2) * diff_histogram[j]

        #### USING NUMPY METHODS
        # indexes = np.arange(len(diff_histogram))
        # indexes_to2 = np.multiply(indexes,indexes)
        # multiply = np.multiply(indexes_to2, diff_histogram)
        # result = np.sum(multiply)

        result = 0
        for key, value in diff_histogram.items():
            result += (key**2) * value

        return result

    def calc_HOMOGENEITY(self, diff_histogram):

        #### USING PYTHON METHODS
        # result = 0
        # for j in range(len(diff_histogram)):
        #     result += (diff_histogram[j]/(1+(j**2)))

        #### USING NUMPÝ METHODS
        # indexes = np.arange(len(diff_histogram))
        # indexes_to2 = np.multiply(indexes, indexes)
        # division = np.divide(diff_histogram,1+indexes_to2)
        # result = np.sum(division)

        result = 0
        for key, value in diff_histogram.items():
            result += value/(1+(key**2))
        return result

    def calc_CORRELATION(self,sum_histogram, diff_histogram, mean):

        #### USING PYTHON METHODS
        # start = timer()
        # result_sum = 0
        # sum_mean = np.mean(sum_histogram)
        # for i in range(len(sum_histogram)):
        #     result_sum += ((i-(2 * sum_mean))**2) * sum_histogram[i]
        #
        # result_diff = self.calc_CONTRAST(diff_histogram)
        #
        # correlation = 0.5*(result_sum - result_diff)
        # end = timer()
        # print("Time using python methods: ", end - start)


        #### USING NUMPY METHODS
        #start = timer()
        # indexes = np.arange(len(diff_histogram))
        # sum_mean = np.mean(sum_histogram)
        # sum_mean_x2 = np.multiply(sum_mean, 2)
        # aux = np.subtract(indexes,sum_mean_x2)
        # i_x_sum_mean_to2 = np.multiply(aux,aux)
        #
        # result_sum = np.multiply(i_x_sum_mean_to2, sum_histogram)
        # result_diff = self.calc_CONTRAST(diff_histogram)
        #
        # aux = np.subtract(result_sum, result_diff)
        # aux = np.multiply(0.5, aux)
        # correlation = np.sum(aux)
        #end = timer()
        #print("Time using numpy methods: ", end - start)


        ###### Equations based on Unser
        result_sum = 0
        for key, value in sum_histogram.items():
            result_sum += ((key - (2 * mean)) ** 2) * value

        result_diff = 0
        for key, value in diff_histogram.items():
            result_diff += (key**2) * value

        result =  (result_sum - result_diff) / 2
        #result = (result_sum / 2 ) - result_diff
        #a = 1

        ###### Equations based on Scikit Image
        # sum_std, diff_std = self.calc_STD(sum_histogram, diff_histogram)
        # result_sum = 0
        # for key, value in sum_histogram.items():
        #     result_sum += (((key - (2 * mean)) ** 2) / (sqrt(sum_std))) * value
        #
        # result_diff = 0
        # for key, value in diff_histogram.items():
        #     result_diff += ((key **2) / sqrt(diff_std)) * value
        #
        # result =  (result_sum - result_diff)
        # return [result, result_sum, result_diff]

        ###### Equations basead on https://www.hindawi.com/journals/ijbi/2015/267807/
        # x_mean, y_mean, x_std, y_std = 0, 0, 0, 0
        #
        # for key, value in sum_histogram.items():
        #     x_mean += key * value
        #
        # for key, value in diff_histogram.items():
        #      y_mean += abs(key) * value
        #
        # x_mean = x_mean/sum(sum_histogram.values())
        # y_mean = y_mean/sum(diff_histogram.values())
        #
        # ####
        #
        # for key, value in sum_histogram.items():
        #     x_std += ( mathPow((key - (2 * mean)), 2)) * value
        #
        # for key, value in diff_histogram.items():
        #     y_std += ( mathPow((key - y_mean), 2)) * value
        # x_std, y_std = sqrt(x_std), sqrt(y_std)
        #
        #
        # ####
        #
        # result_sum = 0
        # for key, value in sum_histogram.items():
        #     result_sum += (key- x_mean) / x_std
        #
        # result_diff = 0
        # for key, value in diff_histogram.items():
        #     result_diff += (key - y_mean) / y_std
        #
        # result = (2 * abs(result_sum)) - (2 * abs(result_diff))
        # return [result, result_sum, result_diff]

        return result

    def calc_CORRELATION_SCIKIT(self, sum_histogram, diff_histogram, mean, meanJ, gray_levels):

        #I = np.asarray(list(sum_histogram.keys()))
        I = np.array(range(gray_levels)).reshape((gray_levels, 1))
        J = np.array(range(gray_levels)).reshape((1, gray_levels))

        values = np.asarray(list(sum_histogram.values()))

        num = (I - (mean)) * (I - (mean))
        std_sum = self.calc_SUM_STD(sum_histogram,gray_levels)
        std_diff = self.calc_SUM_STD(diff_histogram,gray_levels)
        std_sum_final = ((std_sum + std_diff)/4)
        std_diff_final = ((std_sum + std_diff) / 4)
        #den = np.sqrt(self.calc_SUM_STD(sum_histogram,gray_levels))# * self.calc_SUM_STD(diff_histogram,gray_levels)  ) # * self.calc_J_STD(glcm,gray_levels))
        den = np.sqrt((std_sum_final * std_diff_final))
        aux = num/den
        x = np.copy(values)
        x.resize(values.shape,refcheck=False)





        sum_dem = np.sum(den)
        sum_hist = np.sum(x)
        numDem = np.sum(num / den)
        iMeanI = np.sum(I - mean)
        sum_num = np.sum(num)
        #quo = np.sum((values * num) / den)

        quo = np.sum(x * (num / den))


        ####################
        # I = np.asarray(list(sum_histogram.keys()))
        # num = I - mean
        # den = self.calc_SUM_STD(sum_histogram,gray_levels)
        # quoSUM = np.sum(values * (num / den))
        #
        # J = np.asarray(list(diff_histogram.keys()))
        # values = np.asarray(list(diff_histogram.values()))
        # num = J - meanJ
        # den = self.calc_SUM_STD(diff_histogram,gray_levels)
        # quoDIF = np.sum(values * (num / den))
        # quo = quoSUM *  quoDIF
        # std_sum = self.calc_SUM_STD(sum_histogram, gray_levels)
        # std_diff = self.calc_SUM_STD(diff_histogram, gray_levels)
        # std_sum_final = ((std_sum + std_diff) / 4)
        # std_diff_final = ((std_sum + std_diff) / 4)
        # den = np.sqrt(self.calc_SUM_STD(sum_histogram,gray_levels))# * self.calc_SUM_STD(diff_histogram,gray_levels)  ) # * self.calc_J_STD(glcm,gray_levels))
        # den = np.sqrt((std_sum_final * std_diff_final) / 4)
        # quo = np.sum(values * (num / (den)))


        ################# Tradicional
        # result = 0
        # std_sum_final = ((std_sum + std_diff) /4)
        # std_diff_final = ((std_sum + std_diff)/4 )
        # den = np.sqrt((std_sum_final * std_diff_final))
        # for key, value in sum_histogram.items():
        #     num = (key - mean) * (key - mean)
        #     result += (num/den) * value


        ############# Tradicional dois laços
        # result = 0
        # std_sum_final = ((std_sum + std_diff) /4)
        # std_diff_final = ((std_sum + std_diff)/4 )
        # den = np.sqrt((std_sum_final **2))*2

        resultSum = 0
        denSum = np.sqrt(self.calc_SUM_STD(sum_histogram,gray_levels))
        for key, value in sum_histogram.items():
            num = (key - (2*mean))**2
            resultSum += (num/denSum) * value

        resultDiff = 0
        denDiff = np.sqrt(self.calc_SUM_STD(diff_histogram, gray_levels))
        for key, value in diff_histogram.items():
            num = (key - meanJ)
            resultDiff += (num / denDiff) * value

        result = resultSum * resultDiff

        ###### Equations based on Unser
        result_sum = 0
        for key, value in sum_histogram.items():
            result_sum += ((key - (2 * mean)) ** 2) * value

        result_diff = 0
        for key, value in diff_histogram.items():
            result_diff += (key **2) * value


        std_sum = self.calc_SUM_STD(sum_histogram,gray_levels)
        std_diff = self.calc_SUM_STD(diff_histogram,gray_levels)
        std_sum_final = ((std_sum + std_diff)/4)
        std_diff_final = ((std_sum + std_diff) / 4)

        result_2 = ((result_sum - result_diff) / (std_diff_final)) #/ 4
        #result_2 = ((result_sum - result_diff) / (std_sum + std_diff)**2 / 4)
        # if self.level != "0º":
        #     result_2 = (result_2 / 10 ) + 1

        #return quo, result, result_2
        return result_2/4



    def calc_SUM_STD(self, sum_histogram, gray_levels):
        values = np.asarray(list(sum_histogram.values()))
        I = np.asarray(list(sum_histogram.keys()))
        #diff_i = I - self.calc_I_MEAN_(sum_histogram, gray_levels)
        diff_i = I - np.sum(I * values)
        std_i = np.sum(values * (diff_i) ** 2)
        #std_i = np.sum(values * (diff_i))
        #std_i = np.sum(values * diff_i ** 2)

        return std_i


    def calc_DISSIMILARITY(self,diff_histogram):

        #### USING PYTHON METHODS
        # dissimilarity = 0
        # for j in range(len(diff_histogram)):
        #     dissimilarity += j * diff_histogram[j]

        #### USING NUMPY METHODS
        # indexes = np.arange(len(diff_histogram))
        # dissimilarity = np.multiply(indexes,diff_histogram)
        # dissimilarity = np.sum(dissimilarity)

        result = 0
        for key, value in diff_histogram.items():
            result += abs(key) * value

        return result

    def calc_MEAN(self, sum_histogram, gray_levels):
        sum_values = sum(sum_histogram.values())
        result = sum_values/gray_levels
        return result

    def calc_I_MEAN_(self, sum_histogram, gray_levels):
        values = np.asarray(list(sum_histogram.values()))
        I = np.asarray(list(sum_histogram.keys()))
        result = np.sum(I * values)/2
        return result

    def calc_STD(self, sum_histogram, diff_histogram):

        sum_std = np.std(np.array(list(sum_histogram.values())))
        diff_std = np.std(np.array(list(diff_histogram.values())))

        return [sum_std, diff_std]

    def calc_ENTROPY(self, sum_histogram, diff_histogram):
        result, result_sum, result_diff = 0,0,0

        for key, value in sum_histogram.items():
            result_sum += (value) * log10(value)

        for key, value in diff_histogram.items():
            result_diff += (value) * log10(value)

        result = ((result_sum * -1) - (result_diff))
        return result

    def calc_VARIANCE(self,sum_histogram, diff_histogram, mean):
        '''Na proposta de Unser(86) o resultado é divido por 2. Aqui dividimos por 4'''
        result_sum = 0
        for key, value in sum_histogram.items():
            result_sum += ((key - ( 2 * mean))**2) * (value)

        result_diff = 0
        for key, value in diff_histogram.items():
            result_diff += (key**2) * (value)

        #result = (result_sum /2 )+ result_diff
        result = (result_sum + result_diff) / 2
        #result = result /2
        return result

    def calc_CLUSTER_SHADE(self, sum_histogram, mean):
        result = 0
        for key, value in sum_histogram.items():
            result += ((key - (2 * mean))**3) * value
        return result

    def calc_CLUSTER_PROMINENCE(self, sum_histogram, mean):
        result = 0
        for key, value in sum_histogram.items():
            result += ((key - (2 * mean)) ** 4) * value
        return result



if __name__ == '__main__':
    image = np.array([(0,0,0,0,0), (0,0,0,0,0), (1,1,1,1,1), (1,1,1,1,1)])
    #image = astronaut()
    #image = np.array([(1,2,3,4,5), (6,7,8,9,10), (11,12,13,14,15), (16,17,18,19,20)])

    #image = imread("img.jpg")

    scaler = MinMaxScaler(feature_range=(0, 255))
    imageX = rgb2grey(astronaut())
    imageX = scaler.fit_transform(imageX)
    imageX = imageX.astype(int)
    main = SumDiffHistogram(imageX, 0, 1)