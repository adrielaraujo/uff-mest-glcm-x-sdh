from Core.SDH import SumDiffHistogram
from GetProcessingTime import GetProcessingTime

from skimage.data import astronaut
from skimage.color import rgb2grey
from skimage.feature import greycoprops, greycomatrix

from sklearn.preprocessing import MinMaxScaler

from timeit import default_timer as timer

class Main():

    def __init__(self):
        self.runAndCompare()
        # self.descriptorsLogGenerated = False
        # self.getTime = GetProcessingTime()
        # self.generateTimesLog()

    def generateTimesLog(self):
        fileStructures = "OurXScikit_Structures_NORMAL_v1.csv"
        fileDescriptors = "OurXScikit_Descriptors_NORMAL_v1.csv"
        image = self.imagesToTest()

        self.generateStructuresTimeLog(image, fileStructures)

        self.getTime.saveCSV(fileDescriptors, self.generateDescriptorsTimeLog(image, fileDescriptors, 0, 1))
        self.getTime.saveCSV(fileDescriptors, self.generateDescriptorsTimeLog(image, fileDescriptors, 1, 1))
        self.getTime.saveCSV(fileDescriptors, self.generateDescriptorsTimeLog(image, fileDescriptors, 1, 0))
        self.getTime.saveCSV(fileDescriptors, self.generateDescriptorsTimeLog(image, fileDescriptors, 1, -1))

    def generateStructuresTimeLog(self, image, file):

        header = ["", "Our Time - 0", "Scikit Time - 0", "Difference - 0",
                  "Our Time - 45", "Scikit Time - 45", "Difference - 45",
                  "Our Time - 90", "Scikit Time - 90", "Difference - 90",
                  "Our Time - 135", "Scikit Time - 135", "Difference - 135"]

        self.getTime.saveCSV(file, header)
        result = ["Computing Time"]
        result += self.calcStructuresComputingTime(image, 0, 1)
        result += self.calcStructuresComputingTime(image, 1, 1)
        result += self.calcStructuresComputingTime(image, 1, 0)
        result += self.calcStructuresComputingTime(image, 1, -1)

        self.getTime.saveCSV(file, result)

    def generateDescriptorsTimeLog(self, image, file, di, dj):

        if not self.descriptorsLogGenerated:
            header = ["ASM - HSD", "ASM - GLCM", "ASM - X",
                       "ENERGY - HSD", "ENERGY - GLCM", "ENERGY - X",
                       "CONTRAST - HSD", "CONTRAST - GLCM", "CONTRAST - X",
                       "DISSIMILARITY - HSD", "DISSIMILARITY - GLCM", "DISSIMILARITY - X",
                       "HOMOGENEITY - HSD", "HOMOGENEITY - GLCM", "HOMOGENEITY - X",
                       "CORRELATION - HSD", "CORRELATION - GLCM", "CORRELATION - X"]
            self.getTime.saveCSV(file, header)
            self.descriptorsLogGenerated = True

        if abs(di) < abs(dj): level = 0
        elif abs(di) > abs(dj): level = 90
        elif di == dj and dj>0: level = 45
        else: level = 135

        SDHCalculator = SumDiffHistogram()

        sum_occurrences, diff_occurences = SDHCalculator.prepare_image(image, di, dj)
        glcm = greycomatrix(image, [1], [level], levels=255, normed=True)
        asmHSD = SDHCalculator.calc_ASM(sum_occurrences)
        grey_levelsHSD = SDHCalculator.get_gray_levels(image)
        meanHSD = SDHCalculator.calc_MEAN(sum_occurrences, grey_levelsHSD)


        result = []
        #### ASM
        timeOUR = self.getTime.getTime(SDHCalculator.calc_ASM, (sum_occurrences,))
        timeSCIKIT = self.getTime.getTime(greycoprops, (glcm, "ASM",))
        print(timeOUR, timeSCIKIT)
        print(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))
        result.append(timeOUR)
        result.append(timeSCIKIT)
        result.append(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))

        #### ENERGY
        timeOUR = self.getTime.getTime(SDHCalculator.calc_ENERGY, (asmHSD,))
        timeSCIKIT = self.getTime.getTime(greycoprops, (glcm, "energy",))
        print(timeOUR, timeSCIKIT)
        print(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))
        result.append(timeOUR)
        result.append(timeSCIKIT)
        result.append(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))

        #### CONTRAST
        timeOUR = self.getTime.getTime(SDHCalculator.calc_CONTRAST, (diff_occurences,))
        timeSCIKIT = self.getTime.getTime(greycoprops, (glcm, "contrast",))
        print(timeOUR, timeSCIKIT)
        print(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))
        result.append(timeOUR)
        result.append(timeSCIKIT)
        result.append(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))

        #### DISSIMILARITY
        timeOUR = self.getTime.getTime(SDHCalculator.calc_DISSIMILARITY, (diff_occurences,))
        timeSCIKIT = self.getTime.getTime(greycoprops, (glcm, "dissimilarity",))
        print(timeOUR, timeSCIKIT)
        print(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))
        result.append(timeOUR)
        result.append(timeSCIKIT)
        result.append(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))

        #### HOMEGENEITY
        timeOUR = self.getTime.getTime(SDHCalculator.calc_HOMOGENEITY, (diff_occurences,))
        timeSCIKIT = self.getTime.getTime(greycoprops, (glcm, "homogeneity",))
        print(timeOUR, timeSCIKIT)
        print(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))
        result.append(timeOUR)
        result.append(timeSCIKIT)
        result.append(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))

        #### CORRELATION
        timeOUR = self.getTime.getTime(SDHCalculator.calc_CORRELATION, (sum_occurrences, diff_occurences, meanHSD))
        timeSCIKIT = self.getTime.getTime(greycoprops, (glcm, "correlation",))
        print(timeOUR, timeSCIKIT)
        print(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))
        result.append(timeOUR)
        result.append(timeSCIKIT)
        result.append(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))

        return  result

    def calcStructuresComputingTime(self, image, di, dj, border=False):

        #############################################
        ####     Structures Computing  Time      ####
        #############################################

        if abs(di) < abs(dj): level = 0
        elif abs(di) > abs(dj): level = 90
        elif di == dj and dj>0: level = 45
        else: level = 135

        SDHCalculator = SumDiffHistogram()

        timeSCIKIT = self.getTime.getTime(greycomatrix, (image, [1], [level], 255, False, True))
        timeOUR = self.getTime.getTime(SDHCalculator.prepare_image, (image, di, dj))  # 0º

        difference = self.getTime.calcDiffBetweenTimes(timeSCIKIT, timeOUR)

        return [timeOUR, timeSCIKIT, difference]

    def runAndCompare(self):

        image = self.imagesToTest()

        start = timer()
        g = greycomatrix(image, [1], [0], levels=255, normed = True)
        end = timer()
        time_glcm = end - start


        start = timer()
        ASM = greycoprops(g, 'ASM')
        end = timer()
        time_asm =  end - start

        start = timer()
        energy = greycoprops(g, 'energy')
        end = timer()
        time_energy =  end - start

        start = timer()
        contrast = greycoprops(g, 'contrast')
        end = timer()
        time_contrast =  end - start

        start = timer()
        dissimilarity = greycoprops(g, 'dissimilarity')
        end = timer()
        time_diss =  end - start

        start = timer()
        homogeneity = greycoprops(g, 'homogeneity')
        end = timer()
        time_homo =  end - start

        start = timer()
        correlation = greycoprops(g, 'correlation')
        end = timer()
        time_corr =  end - start

        print("GLCM\nASM", ASM, time_asm, "\nEnergy", energy, time_energy, "\nContrast", contrast, time_contrast,
              "\nDissimilarity", dissimilarity, time_diss, "\nHomogeneity", homogeneity,  time_homo,
              "\nCorrelation", correlation, time_corr, "\nTime GLCM", time_glcm)

        print("Time using SCIKIT methods: ", end - start)

        #################################

        SDHCalculator = SumDiffHistogram()
        start = timer()
        sum_occurences, diff_occurences = SDHCalculator.prepare_image(image, 0, 1)
        end = timer()
        time_hsd = end-start

        start = timer()
        asm = SDHCalculator.calc_ASM(sum_occurences)  # self.calc_Total_ASM(sum_occurences,diff_occurences)#
        end = timer()
        time_asm2 = end - start

        start = timer()
        energy = SDHCalculator.calc_ENERGY(asm)
        end = timer()
        time_energy2 = end - start

        start = timer()
        contrast = SDHCalculator.calc_CONTRAST(diff_occurences)
        end = timer()
        time_contrast2 = end - start

        start = timer()
        dissimilarity = SDHCalculator.calc_DISSIMILARITY(diff_occurences)
        end = timer()
        time_diss2 = end - start

        start = timer()
        homogeneity = SDHCalculator.calc_HOMOGENEITY(diff_occurences)
        end = timer()
        time_homo2 = end - start

        mean = SDHCalculator.calc_MEAN(sum_occurences, SDHCalculator.get_gray_levels(image))

        start = timer()
        correlation = SDHCalculator.calc_CORRELATION(sum_occurences, diff_occurences, mean)
        end = timer()
        time_corr2 = end - start

        print("\n\nHSD\nASM", asm, time_asm2, "\nEnergy", energy, time_energy2, "\nContrast", contrast, time_contrast2,
              "\nDissimilarity", dissimilarity, time_diss2, "\nHomogeneity", homogeneity, time_homo2,
              "\nCorrelation", correlation, time_corr2, "\nTimeHSD", time_hsd)

        print("Time using PYTHON methods: ", end - start)

        print("\n","\n",time_asm > time_asm2,"\n",
              time_energy > time_energy2,"\n",
              time_contrast > time_contrast2,"\n",
              time_diss > time_diss2,"\n",
              time_homo > time_homo2,"\n",
              time_corr > time_corr2, "\n",
              time_glcm > time_hsd)

    def imagesToTest(self):

        '''
        image1 = np.array([(0, 0, 0, 0), (0, 0, 0, 0), (1, 1, 1, 1), (1, 1, 1, 1)])
        image2 = np.array([(0, 0, 1, 1), (0, 0, 1, 1), (0, 0, 1, 1), (0, 0, 1, 1)])
        image3 = np.array(
            [(0, 0, 0, 0, 0, 0), (0, 0, 0, 0, 0, 0), (1, 1, 1, 1, 1, 1), (1, 1, 1, 1, 1, 1), (2, 2, 2, 2, 2, 2),
             (2, 2, 2, 2, 2, 2)])

        image4 = np.array([(0, 0, 0, 0, 0, 0, 0, 0),
                           (0, 0, 0, 0, 0, 0, 0, 0),
                           (1, 1, 1, 1, 1, 1, 1, 1),
                           (1, 1, 1, 1, 1, 1, 1, 1),
                           (2, 2, 2, 2, 2, 2, 2, 2),
                           (2, 2, 2, 2, 2, 2, 2, 2),
                           (3, 3, 3, 3, 3, 3, 3, 3),
                           (3, 3, 3, 3, 3, 3, 3, 3)])

        image5 = np.array([(0, 1, 2, 3, 0, 1, 2, 3),
                           (0, 1, 2, 3, 0, 1, 2, 3),
                           (0, 1, 2, 3, 0, 1, 2, 3),
                           (0, 1, 2, 3, 0, 1, 2, 3),
                           (0, 1, 2, 3, 0, 1, 2, 3)])

        image6 = np.array([(0, 0, 0, 0, 0, 0, 0, 0),
                           (1, 1, 1, 1, 1, 1, 1, 1),
                           (2, 2, 2, 2, 2, 2, 2, 2),
                           (3, 3, 3, 3, 3, 3, 3, 3)])

        image7 = np.array([(0, 0, 0, 1, 1, 0, 0, 0),
                           (0, 0, 0, 0, 0, 0, 0, 0),
                           (1, 1, 1, 1, 3, 3, 1, 1),
                           (1, 1, 2, 2, 1, 1, 1, 1),
                           (2, 2, 0, 0, 2, 2, 2, 2),
                           (3, 3, 2, 2, 2, 2, 2, 2),
                           (3, 3, 3, 3, 1, 1, 3, 3),
                           (2, 2, 3, 3, 3, 3, 3, 3)])

        '''
        scaler = MinMaxScaler(feature_range=(0, 255))
        imageX = rgb2grey(astronaut())
        imageX = scaler.fit_transform(imageX)
        imageX = imageX.astype(int)

        '''
        imageTireoide = rgb2grey(imread("imgtireoide.jpg"))
        imageTireoide = scaler.fit_transform(imageTireoide)
        imageTireoide = imageTireoide.astype(int)

        imageMama = rgb2grey(imread("imagMama.PNG"))
        imageMama = scaler.fit_transform(imageMama)
        imageMama = imageMama.astype(int)

        imageMamaDireita = rgb2grey(imread("imgMamaRight.PNG"))
        imageMamaDireita = scaler.fit_transform(imageMamaDireita)
        imageMamaDireita = imageMamaDireita.astype(int)
        '''

        image = imageX

        return image



if __name__ == "__main__":
    program = Main()




    # imageMama =   np.loadtxt("IR_0810.txt")#, dtype='f8') # dtype='f3' is float 32 ;;; 'f8' is float 64
    # mask_right = rgb2grey(imread("IR_0810-dir.png", flatten=True))
    # mask_left = rgb2grey(imread("IR_0810-esq.png", flatten=True))
    # img_left = np.copy(imageMama)
    # img_right = np.copy(imageMama)
    #
    # img_left[mask_left == 0] = 0
    # img_right[mask_right == 0] = 0
    #
    #
    # img_left = rgb2grey(img_left)
    #
    # plt.imshow(imageMama, cmap='gray')
    # plt.show()