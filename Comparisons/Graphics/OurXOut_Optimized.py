from Core.SDH import SumDiffHistogram
from Core.GLCM import GLCM_Descriptors
from Utils.images import *
from Comparisons.Graphics.Common import GraphicsCore


class GraphComparison:

    def __init__(self):

        image = imagesToTest()
        di = 0
        dj = 1

        SDH_Results, GLCM_Results = self.get_Results(image, di, dj)
        GraphicsCore(SDH_Results,GLCM_Results)

        print("Done!")

    def get_Results(self, image, di, dj):

        level = convertToAngle(di, dj)

        SDH_Results = SumDiffHistogram(image, di, dj)



        return  SDH_Results,GLCM_Results
