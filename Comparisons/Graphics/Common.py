import matplotlib.pyplot as plt
import numpy as np

class GraphicsCore:
    def __init__(self, Our_SDH, Unser_SDH, GLCM_Results = None):

        #self.plot_Graphics(Our_SDH, Unser_SDH, GLCM_Results)
        #self.plot_BarChat(Our_SDH, Unser_SDH, GLCM_Results)
        #self.plot_Scatter()
        self.plotBars(Our_SDH, Unser_SDH, GLCM_Results)

    def plotBars(self, Our_SDH, Unser_SDH, GLCM_Results):
        import seaborn as sns
        import matplotlib.pyplot as plt


        x_axis = ["ENE", "DPA-J", "MED-I", "CSHA", "CPRO", "COR-1"]
        y_axis = [78.5, 79.6, 81.6, 75.4, 78.3, 79.6]

        plt.ylabel('Accuracy')
        plt.title('Accuracy of Classifier')

        g = sns.barplot(x_axis, y_axis, color="red")
        ax = g
        # annotate axis = seaborn axis
        for p in ax.patches:
            ax.annotate("%.2f" % p.get_height(), (p.get_x() + p.get_width() / 2., p.get_height()),
                        ha='center', va='center', fontsize=11, color='gray', xytext=(0, 20),
                        textcoords='offset points')
        _ = g.set_ylim(0, 120)  # To make space for the annotations

        plt.show()

    def plot_Graphics(self, Our_SDH, Unser_SDH, GLCM_Results):


        # Some sample lists
        #print("SDH\n",SDH_Results.features, "\nGLCM\n", GLCM_Results.features)
  #      l1 = Our_SDH.features  # [200,200,200,200,200,200]# AZUL
 #       l2 = Unser_SDH.features
#        l3 = GLCM_Results.features
#
#         l1 = [ 7300.04,
#  479.04
# ]
#         l2 = [ 14185.86,
#  2257.86
# ]
#         l3 = [ 7900.26,
#  479.04
#
# ]

        #l1 = l1/sum(l1)
        #l2 = l2/sum(l2)
       # l2[0] = l2[0] * -1
        #l1 = l1[4:5]
        #l2 = l2[4:5]
        #l3 = l3[4:5]

        #l1 = [i / sum(l1) for i in l1]
        #l2 = [i / sum(l2) for i in l2]

       # Make a figure
        l1 = Our_SDH, l2 = Unser_SDH
        fig = plt.figure()

        # Make room for legend at bottom
        fig.subplots_adjust(bottom=0.2)

        # The axes for your lists 1-3
        ax1 = fig.add_subplot(111)

        # Plot lines 1-3
        #line4 = ax1.plot(l2, 'y-', label='list 3')
        #line1 = ax1.plot(l1, 'bs', label='list 1')
        #line2 = ax1.plot(l2, 'g^', label='list 2')
        #line3 = ax1.plot(l3, 'g.', label='list 2')


        #line3 = ax1.plot(l3, 'gD-', label='list 3', linewidth=5.0, markersize=10.0)
        line1 = ax1.plot(l1, 'bs--', label='list 1', linewidth=5.0, markersize=10.0)
        line2 = ax1.plot(l2, 'yo--', label='list 2', linewidth=5.0, markersize=10.0)

        # if len(l3) > 0:
        #     line2 = ax1.plot(l2, 'go-', label='list 2')
        #     line3 = ax1.plot(l3, 'ro-', label='list 3')
        # else:
        #     line2 = ax1.plot(l3, 'ro-', label='list 3')
        # line3 = ax1.plot(l3, 'ro-', label='list 3')


        # To get lines 1-5 on the same legend, we need to gather all the lines together before calling legend
        lines = line1 + line2  # + line3 + line4 + line5
        #labels = [l.get_label() for l in lines]
        #labels = ["asm", " energy"]#, " entropy", " contrast", " dissimilarity", " homogeneity", " meanI", " correlation", " variance", " cluster_shade", "cluster_prominence"]
        #plt.plot(l1, l1, 'bo-', l2, l2, 'go-')#, t, t ** 3, 'g^')
        ax1.set_xticklabels(('', 'COR1', "", "", "", "", "VAR"))
        plt.title("EXPERIMENT 1 - ANGLE 0º")
        plt.show()


    def plot_BarChat(self,Our_SDH, Unser_SDH, GLCM_Results):
         #7#

        # our_values = Our_SDH.features#[ 0, 0, 0.0025, 0.3250, 0.3225, 0, 0]#
        # unser_values = Unser_SDH.features#[844.39, 74358.42, 0.0025, 0.9975, 15733.325, 1562.10, 0.0050]#
        # glcm_values = GLCM_Results.features

        #[ correlation, variance, stdI, stdJ, cluster_shade, cluster_prominence]
        # glcm_values = [836.236249, 987.830957, 987.830957, 986.790168]#, -172953.730572, 47492548.39435]
        # our_values =  [836.236249, 987.310564, 987.310564, 987.310564]#, -172953.730572, 47492548.394351]
        # unser_values =[1672.472496, 1974.621126, -0,0]#, -172933.397597, 47491262.820126]
        #
        # HS = {0.0: 2, 1.0: 1, 2.0: 1, 3.0: 5, 4.0: 2, 5.0: 1, 6:0}
        # HD =  {-3.0: 2, -2.0: 2, -1.0: 2, 0.0: 2, 1.0: 2, 2.0: 1, 3.0: 1}
        # hs = HS.values()
        # hd = HD.values()

        fig, ax = plt.subplots()

        bar_width = 0.8
        n_groups = len(hd)
        index = np.arange(n_groups)

        opacity = 0.4
        error_config = {'ecolor': '0.3'}

        rects2 = ax.bar(index, hd, bar_width,
                        alpha=opacity, color='g',
                        error_kw=error_config,
                        )

        # rects1 = ax.bar(index + bar_width, hd, bar_width,
        #                 alpha=opacity, color='b',
        #                 error_kw=error_config,
        #                 )

        # rects3 = ax.bar(index + 2*bar_width, glcm_values, bar_width,
        #                 alpha=opacity, color='g',
        #                 error_kw=error_config,
        #                )

        #ax.set_xlabel('Descriptors')
       # ax.set_ylabel('Values')
        #ax.set_title('Scores by group and gender')
        #ax.set_xticks((index + 3*bar_width / 3) + bar_width/2)
        ax.set_xticks(index)
        #ax.set_xticklabels(('ASM', 'ENE', 'HOM'))
        #ax.set_xticklabels(('COR-2', 'ENT', 'DIS'))
        #ax.set_xticklabels(('CON', 'AVE-I', 'AVE-J', 'AVE-G'))
        #ax.set_xticklabels(('COR-1', 'VAR', 'STD-I', 'STD-J'))
        ax.set_xticklabels(('-3','-2','-1','0','1', '2', '3'))
        #ax.set_xticklabels(('CSHA'))
        ax.legend()

        fig.tight_layout()
        plt.show()


    def plot_Scatter(self):

        o1 = [4.6, 3.85, 4.19]
        u1 = [4.6, 3.85, 4.19]
        g1 = [4.24, 3.31, 3.68]

        u2 = [5224.822777, 1417.647957, 2250.494614]
        u3 = [ 4475.016281, 867.44652, 1727.25308]
        u4 = [ 126.327962, 31.043861, 147.79179]
        u5 = [ -119456.357656, 506106.834156, -256424.915651]
        u6 = [ 205979121.514656, 157156849.752441, 58261359.510734]



        o2 = [ 2612.412516, 708.82398, 1125.248548]
        o3 = [2237.509269, 433.723262, 863.627781]
        o4 = [126.361548, 31.042518, 147.82702]
        o5 = [-121411.023793, 506125.244456, -257265.714091]
        o6 = [206011480.487343, 157162286.579743, 58333747.225863]


        g2 = [2612.50027, 709.770834, 1123.990308]
        g3 = [2237.509269, 433.723262, 863.627781]
        g4 = [126.361548, 31.042518, 147.82702]
        g5 = [-121411.023793, 506125.244456, -257265.714091]
        g6 = [206011480.487341, 157162286.579743, 58333747.225862]

        
        #entropy, variance, correlation, meanI, cluster_shade, cluster_prominence

        # Fixing random state for reproducibility


        np.random.seed(19680801)

        x = np.random.rand(3)
        y = np.random.rand(3)
        z = np.sqrt(x ** 2 + y ** 2)

        plt.subplot(321)
        plt.scatter(x, o1,  c='b', marker=">")
        plt.scatter(x, u1,  c='y', marker="+")
        plt.scatter(x, g1, c='g', marker="*")
        plt.title("ENT")

        plt.subplot(322)
        plt.scatter(x, o2,  c='b', marker=">")
        plt.scatter(x, u2,  c='y', marker="+")
        plt.scatter(x, g2, c='g', marker="*")
        plt.title("VAR")

        plt.subplot(323)
        plt.scatter(x, o3,  c='b', marker=">")
        plt.scatter(x, u3,  c='y', marker="+")
        plt.scatter(x, g3, c='g', marker="*")
        plt.title("COR-1")

        plt.subplot(324)
        plt.scatter(x, o4,  c='b', marker=">")
        plt.scatter(x, u4,  c='y', marker="+")
        plt.scatter(x, g4, c='g', marker="*")
        plt.title("AVE-I")

        plt.subplot(325)
        plt.scatter(x, o5,  c='b', marker=">")
        plt.scatter(x, u5,  c='y', marker="+")
        plt.scatter(x, g5, c='g', marker="*")
        plt.title("CSHA")

        plt.subplot(326)
        plt.scatter(x, o6,  c='b', marker=">")
        plt.scatter(x, u6,  c='y', marker="+")
        plt.scatter(x, g6, c='g', marker="*")
        plt.title("CPRO")

        #plt.scatter(x, y, s=80, c='r', marker=">")

        # plt.subplot(322)
        # plt.scatter(x, y, s=80, c=z, marker=(5, 0))
        #
        # verts = np.array([[-1, -1], [1, -1], [1, 1], [-1, -1]])
        # plt.subplot(323)
        # plt.scatter(x, y, s=80, c=z, marker='+')
        # # equivalent:
        # # plt.scatter(x, y, s=80, c=z, marker=None, verts=verts)
        #
        # plt.subplot(324)
        # plt.scatter(x, y, s=80, c=z, marker=(5, 1))
        #
        # plt.subplot(325)
        # plt.scatter(x, y, s=80, c=z, marker='+')
        #
        # plt.subplot(326)
        # plt.scatter(x, y, s=80, c=z, marker=(5, 2))

        plt.show()



if __name__ == "__main__":
    program = GraphicsCore([],[],[])
