from Core.SDH import SumDiffHistogram
from Core.GLCM import GLCM_Descriptors
from Utils.images import imagesToTest, syntheticImagesToPaper1, brodazImagesToPaper1
from Utils.datasets import normalizeDataset
from Comparisons.Graphics.Common import GraphicsCore
import matplotlib.pyplot as plt



class GraphComparison:

    def __init__(self):

        #image = imagesToTest()
        images = syntheticImagesToPaper1()
        #images = brodazImagesToPaper1("../../StaticFiles/paper1/brodatz-1/")

        images = [images[0]]
        for image in images:
            #print("\n",image[0])

            plt.imshow(image,  cmap='gray')
            di = 0
            dj = 1

            SDH_Results, GLCM_Scikit_Results = self.get_Results(image, di, dj)
            GraphicsCore(SDH_Results, GLCM_Scikit_Results)

            di = 1
            dj = 1

            SDH_Results, GLCM_Scikit_Results = self.get_Results(image, di, dj)
            GraphicsCore(SDH_Results, GLCM_Scikit_Results)

            di = 1
            dj = 0

            SDH_Results, GLCM_Scikit_Results = self.get_Results(image, di, dj)
            GraphicsCore(SDH_Results, GLCM_Scikit_Results)

            di = 1
            dj = -1

            SDH_Results, GLCM_Scikit_Results = self.get_Results(image, di, dj)
            GraphicsCore(SDH_Results, GLCM_Scikit_Results)
            print("\n####################################################################")

        print("Done!")

    def get_Results(self, image, di, dj):

        SDH_Results = SumDiffHistogram(image, di, dj, returnJustDifferences=True)

        GLCM_Results = GLCM_Descriptors(image, di, dj, returnJustDifferences=True)



        return SDH_Results,GLCM_Results




if __name__ == "__main__":
    program = GraphComparison()
