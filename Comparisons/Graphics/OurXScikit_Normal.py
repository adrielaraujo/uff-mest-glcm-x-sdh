from Core.SDH import SumDiffHistogram
from Core.Scikit import ScikitDescriptors
from Utils.images import imagesToTest, readImagesFromDir, syntheticImagesToPaper1
from Comparisons.Graphics.Common import GraphicsCore
from Utils.MasterThesisImages import *

class GraphComparison:
    def __init__(self):
        dir = "C:/Users/Aluno/Documents/UFF/Projetos/[UFF-MEST] GLCM x SDH\StaticFiles/brodaz/"
        #images = readImagesFromDir(dir)
        #images = imagesToTest()
        #images = syntheticImagesToPaper1()
        #images = syntheticImages()
        images = ponceImages("../../StaticFiles/Ponce/Dissertação_v2/textures/")

        for image in images:
           # print("------------------------------------------\n",image[0])
            di = 0
            dj = 1

            SDH_Results, GLCM_Scikit_Results = self.get_Results(image, di, dj)
            #GraphicsCore(SDH_Results, GLCM_Scikit_Results)

            di = 1
            dj = 1

            SDH_Results, GLCM_Scikit_Results = self.get_Results(image, di, dj)
            #GraphicsCore(SDH_Results, GLCM_Scikit_Results)

            di = 1
            dj = 0

            SDH_Results, GLCM_Scikit_Results = self.get_Results(image, di, dj)
            #GraphicsCore(SDH_Results, GLCM_Scikit_Results)

            di = 1
            dj = -1

            SDH_Results, GLCM_Scikit_Results = self.get_Results(image, di, dj)
            #GraphicsCore(SDH_Results, GLCM_Scikit_Results)
            print("\n####################################################################")

        print("Done!")

    def get_Results(self, image, di, dj):


        SDH_Results = SumDiffHistogram(image, di, dj, comparingScikit=True)
        GLCM_Results = ScikitDescriptors(image,di, dj)

        return SDH_Results, GLCM_Results




if __name__ == "__main__":
    program = GraphComparison()
