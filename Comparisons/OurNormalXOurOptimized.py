from Core.SDH import SumDiffHistogram as SDH_No
from Core.SDH_Optimized_v1 import  SumDiffHistogram as SDH_Op
from Core.GLCM import GLCM_Descriptors as GLCM_No
from Core.GLCM_Optimized_v1 import GLCM_Descriptors as GLCM_Op
from Comparisons.GetProcessingTime import GetProcessingTime

from skimage.data import astronaut
from skimage.color import rgb2grey
from skimage.feature import greycoprops, greycomatrix

from sklearn.preprocessing import MinMaxScaler

from timeit import default_timer as timer

class Main():

    def __init__(self):
        self.descriptorsLogGenerated = False
        self.getTime = GetProcessingTime()
        #self.generateTimesLog()
        #self.runAndCompareGLCMs()
        self.runAndCompareSDHs()

    def generateTimesLog(self):
        fileStructures = "OurXScikit_Normal_Structures_v1.csv"
        fileDescriptors = "OurXScikit_Normal_Descriptors_v1.csv"
        image = self.imagesToTest()

        self.generateStructuresTimeLog(image, fileStructures)

        self.getTime.saveCSV(fileDescriptors, self.generateDescriptorsTimeLog(image, fileDescriptors, 0, 1))
        self.getTime.saveCSV(fileDescriptors, self.generateDescriptorsTimeLog(image, fileDescriptors, 1, 1))
        self.getTime.saveCSV(fileDescriptors, self.generateDescriptorsTimeLog(image, fileDescriptors, 1, 0))
        self.getTime.saveCSV(fileDescriptors, self.generateDescriptorsTimeLog(image, fileDescriptors, 1, -1))

    def generateStructuresTimeLog(self, image, file):

        header = ["", "Our Normal Time - 0", "Our Optimized Time - 0", "Difference - 0",
                  "Our Normal Time - 45", "Our Optimized Time - 45", "Difference - 45",
                  "Our Normal Time - 90", "Our Optimized Time - 90", "Difference - 90",
                  "Our Normal Time - 135", "Our Optimized Time - 135", "Difference - 135"]

        self.getTime.saveCSV(file, header)
        result = ["Computing Time"]
        result += self.calcStructuresComputingTime(image, 0, 1)
        result += self.calcStructuresComputingTime(image, 1, 1)
        result += self.calcStructuresComputingTime(image, 1, 0)
        result += self.calcStructuresComputingTime(image, 1, -1)

        self.getTime.saveCSV(file, result)

    def generateDescriptorsTimeLog(self, image, file, di, dj):

        if not self.descriptorsLogGenerated:
            header = ["ASM - HSD", "ASM - GLCM", "ASM - X",
                       "ENERGY - HSD", "ENERGY - GLCM", "ENERGY - X",
                       "CONTRAST - HSD", "CONTRAST - GLCM", "CONTRAST - X",
                       "DISSIMILARITY - HSD", "DISSIMILARITY - GLCM", "DISSIMILARITY - X",
                       "HOMOGENEITY - HSD", "HOMOGENEITY - GLCM", "HOMOGENEITY - X",
                       "CORRELATION - HSD", "CORRELATION - GLCM", "CORRELATION - X"]
            self.getTime.saveCSV(file, header)
            self.descriptorsLogGenerated = True

        if abs(di) < abs(dj): level = 0
        elif abs(di) > abs(dj): level = 90
        elif di == dj and dj>0: level = 45
        else: level = 135

        SDHCalculator = SDH_No()

        sum_occurrences, diff_occurences = SDHCalculator.prepare_image(image, di, dj)
        glcm = greycomatrix(image, [1], [level], levels=255, normed=True)
        asmHSD = SDHCalculator.calc_ASM(sum_occurrences)
        grey_levelsHSD = SDHCalculator.get_gray_levels(image)
        meanHSD = SDHCalculator.calc_MEAN(sum_occurrences, grey_levelsHSD)


        result = []
        #### ASM
        timeOUR = self.getTime.getTime(SDHCalculator.calc_ASM, (sum_occurrences,))
        timeSCIKIT = self.getTime.getTime(greycoprops, (glcm, "ASM",))
        print(timeOUR, timeSCIKIT)
        print(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))
        result.append(timeOUR)
        result.append(timeSCIKIT)
        result.append(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))

        #### ENERGY
        timeOUR = self.getTime.getTime(SDHCalculator.calc_ENERGY, (asmHSD,))
        timeSCIKIT = self.getTime.getTime(greycoprops, (glcm, "energy",))
        print(timeOUR, timeSCIKIT)
        print(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))
        result.append(timeOUR)
        result.append(timeSCIKIT)
        result.append(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))

        #### CONTRAST
        timeOUR = self.getTime.getTime(SDHCalculator.calc_CONTRAST, (diff_occurences,))
        timeSCIKIT = self.getTime.getTime(greycoprops, (glcm, "contrast",))
        print(timeOUR, timeSCIKIT)
        print(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))
        result.append(timeOUR)
        result.append(timeSCIKIT)
        result.append(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))

        #### DISSIMILARITY
        timeOUR = self.getTime.getTime(SDHCalculator.calc_DISSIMILARITY, (diff_occurences,))
        timeSCIKIT = self.getTime.getTime(greycoprops, (glcm, "dissimilarity",))
        print(timeOUR, timeSCIKIT)
        print(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))
        result.append(timeOUR)
        result.append(timeSCIKIT)
        result.append(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))

        #### HOMEGENEITY
        timeOUR = self.getTime.getTime(SDHCalculator.calc_HOMOGENEITY, (diff_occurences,))
        timeSCIKIT = self.getTime.getTime(greycoprops, (glcm, "homogeneity",))
        print(timeOUR, timeSCIKIT)
        print(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))
        result.append(timeOUR)
        result.append(timeSCIKIT)
        result.append(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))

        #### CORRELATION
        timeOUR = self.getTime.getTime(SDHCalculator.calc_CORRELATION, (sum_occurrences, diff_occurences, meanHSD))
        timeSCIKIT = self.getTime.getTime(greycoprops, (glcm, "correlation",))
        print(timeOUR, timeSCIKIT)
        print(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))
        result.append(timeOUR)
        result.append(timeSCIKIT)
        result.append(self.getTime.calcDiffBetweenTimes(timeOUR, timeSCIKIT))

        return  result

    def calcStructuresComputingTime(self, image, di, dj, border=False):

        #############################################
        ####     Structures Computing  Time      ####
        #############################################

        if abs(di) < abs(dj): level = 0
        elif abs(di) > abs(dj): level = 90
        elif di == dj and dj>0: level = 45
        else: level = 135

        SDHCalculator = SumDiffHistogram()

        timeSCIKIT = self.getTime.getTime(greycomatrix, (image, [1], [level], 255, False, True))
        timeOUR = self.getTime.getTime(SDHCalculator.prepare_image, (image, di, dj))  # 0º

        difference = self.getTime.calcDiffBetweenTimes(timeSCIKIT, timeOUR)

        return [timeOUR, timeSCIKIT, difference]

    def runAndCompareGLCMs(self):

        image = self.imagesToTest()
        GLCM_N = GLCM_No()
        GLCM_O = GLCM_Op()

        GLCMs = [GLCM_N, GLCM_O]
        #################################

        for GLCMCalculator in GLCMs:
            glcm = GLCMCalculator.prepare_image(image, 0, 1)
            mean = GLCMCalculator.calc_MEAN(glcm, GLCMCalculator.get_gray_levels(image))

            start = timer()
            variance = GLCMCalculator.calc_VARIANCE(glcm, mean)
            end = timer()
            time_variance2 = end - start

            start = timer()
            clu_shade = GLCMCalculator.calc_CLUSTER_SHADE(glcm, mean)
            end = timer()
            time_clusterShade2 = end - start

            start = timer()
            clu_pro = GLCMCalculator.calc_CLUSTER_PROMINENCE(glcm, mean)
            end = timer()
            time_clusterPro2 = end - start

            start = timer()
            asm = GLCMCalculator.calc_ASM(glcm)  # self.calc_Total_ASM(sum_occurences,diff_occurences)#
            end = timer()
            time_asm2 = end - start

            start = timer()
            energy = GLCMCalculator.calc_ENERGY(asm)
            end = timer()
            time_energy2 = end - start

            start = timer()
            contrast = GLCMCalculator.calc_CONTRAST(glcm)
            end = timer()
            time_contrast2 = end - start

            start = timer()
            dissimilarity = GLCMCalculator.calc_DISSIMILARITY(glcm)
            end = timer()
            time_diss2 = end - start

            start = timer()
            homogeneity = GLCMCalculator.calc_HOMOGENEITY(glcm)
            end = timer()
            time_homo2 = end - start

            start = timer()
            correlation = GLCMCalculator.calc_CORRELATION(glcm, mean)
            end = timer()
            time_corr2 = end - start

            start = timer()
            entropy = GLCMCalculator.calc_ENTROPY(glcm)
            end = timer()
            time_entro2 = end - start


            print("\nASM", asm, time_asm2, "\nEnergy", energy, time_energy2, "\nContrast", contrast, time_contrast2,
                  "\nDissimilarity", dissimilarity, time_diss2, "\nHomogeneity", homogeneity, time_homo2,
                  "\nCorrelation", correlation, time_corr2, "\nEntropy", entropy, time_entro2,
                  "\nVariance", variance, time_variance2, "\nClu Shade", clu_shade, time_clusterShade2,
                  "\nCluster Prominence", clu_pro, time_clusterPro2, "\nMean", mean,"\n\n-------\n")

    def runAndCompareSDHs(self):

        image = self.imagesToTest()
        SDH_N = SDH_No()
        SDH_O = SDH_Op()

        SDHs = [SDH_N, SDH_O]
        #################################

        for SDHCalculator in SDHs:
            sum_occurrences, diff_occurrences = SDHCalculator.prepare_image(image, 0, 1)

            mean = SDHCalculator.calc_MEAN(sum_occurrences, SDHCalculator.get_gray_levels(image))

            start = timer()
            variance = SDHCalculator.calc_VARIANCE(sum_occurrences,diff_occurrences, mean)
            end = timer()
            time_variance2 = end - start

            start = timer()
            clu_shade = SDHCalculator.calc_CLUSTER_SHADE(sum_occurrences, mean)
            end = timer()
            time_clusterShade2 = end - start

            start = timer()
            clu_pro = SDHCalculator.calc_CLUSTER_PROMINENCE(sum_occurrences, mean)
            end = timer()
            time_clusterPro2 = end - start

            start = timer()
            asm = SDHCalculator.calc_ASM(sum_occurrences)  # self.calc_Total_ASM(sum_occurences,diff_occurences)#
            end = timer()
            time_asm2 = end - start

            start = timer()
            energy = SDHCalculator.calc_ENERGY(asm)
            end = timer()
            time_energy2 = end - start

            start = timer()
            contrast = SDHCalculator.calc_CONTRAST(diff_occurrences)
            end = timer()
            time_contrast2 = end - start

            start = timer()
            dissimilarity = SDHCalculator.calc_DISSIMILARITY(diff_occurrences)
            end = timer()
            time_diss2 = end - start

            start = timer()
            homogeneity = SDHCalculator.calc_HOMOGENEITY(diff_occurrences)
            end = timer()
            time_homo2 = end - start

            start = timer()
            correlation = SDHCalculator.calc_CORRELATION(sum_occurrences, diff_occurrences, mean)
            end = timer()
            time_corr2 = end - start

            start = timer()
            entropy = SDHCalculator.calc_ENTROPY(sum_occurrences)
            end = timer()
            time_entro2 = end - start

            print("\nASM", asm, time_asm2, "\nEnergy", energy, time_energy2, "\nContrast", contrast, time_contrast2,
                  "\nDissimilarity", dissimilarity, time_diss2, "\nHomogeneity", homogeneity, time_homo2,
                  "\nCorrelation", correlation, time_corr2, "\nEntropy", entropy, time_entro2,
                  "\nVariance", variance, time_variance2, "\nClu Shade", clu_shade, time_clusterShade2,
                  "\nCluster Prominence", clu_pro, time_clusterPro2, "\nMean", mean, "\n\n-------\n")

    def imagesToTest(self):

        '''
        image1 = np.array([(0, 0, 0, 0), (0, 0, 0, 0), (1, 1, 1, 1), (1, 1, 1, 1)])
        image2 = np.array([(0, 0, 1, 1), (0, 0, 1, 1), (0, 0, 1, 1), (0, 0, 1, 1)])
        image3 = np.array(
            [(0, 0, 0, 0, 0, 0), (0, 0, 0, 0, 0, 0), (1, 1, 1, 1, 1, 1), (1, 1, 1, 1, 1, 1), (2, 2, 2, 2, 2, 2),
             (2, 2, 2, 2, 2, 2)])

        image4 = np.array([(0, 0, 0, 0, 0, 0, 0, 0),
                           (0, 0, 0, 0, 0, 0, 0, 0),
                           (1, 1, 1, 1, 1, 1, 1, 1),
                           (1, 1, 1, 1, 1, 1, 1, 1),
                           (2, 2, 2, 2, 2, 2, 2, 2),
                           (2, 2, 2, 2, 2, 2, 2, 2),
                           (3, 3, 3, 3, 3, 3, 3, 3),
                           (3, 3, 3, 3, 3, 3, 3, 3)])

        image5 = np.array([(0, 1, 2, 3, 0, 1, 2, 3),
                           (0, 1, 2, 3, 0, 1, 2, 3),
                           (0, 1, 2, 3, 0, 1, 2, 3),
                           (0, 1, 2, 3, 0, 1, 2, 3),
                           (0, 1, 2, 3, 0, 1, 2, 3)])

        image6 = np.array([(0, 0, 0, 0, 0, 0, 0, 0),
                           (1, 1, 1, 1, 1, 1, 1, 1),
                           (2, 2, 2, 2, 2, 2, 2, 2),
                           (3, 3, 3, 3, 3, 3, 3, 3)])

        image7 = np.array([(0, 0, 0, 1, 1, 0, 0, 0),
                           (0, 0, 0, 0, 0, 0, 0, 0),
                           (1, 1, 1, 1, 3, 3, 1, 1),
                           (1, 1, 2, 2, 1, 1, 1, 1),
                           (2, 2, 0, 0, 2, 2, 2, 2),
                           (3, 3, 2, 2, 2, 2, 2, 2),
                           (3, 3, 3, 3, 1, 1, 3, 3),
                           (2, 2, 3, 3, 3, 3, 3, 3)])

        '''
        scaler = MinMaxScaler(feature_range=(0, 255))
        imageX = rgb2grey(astronaut())
        imageX = scaler.fit_transform(imageX)
        imageX = imageX.astype(int)

        '''
        imageTireoide = rgb2grey(imread("imgtireoide.jpg"))
        imageTireoide = scaler.fit_transform(imageTireoide)
        imageTireoide = imageTireoide.astype(int)

        imageMama = rgb2grey(imread("imagMama.PNG"))
        imageMama = scaler.fit_transform(imageMama)
        imageMama = imageMama.astype(int)

        imageMamaDireita = rgb2grey(imread("imgMamaRight.PNG"))
        imageMamaDireita = scaler.fit_transform(imageMamaDireita)
        imageMamaDireita = imageMamaDireita.astype(int)
        '''

        image = imageX

        return image



if __name__ == "__main__":
    program = Main()




    # imageMama =   np.loadtxt("IR_0810.txt")#, dtype='f8') # dtype='f3' is float 32 ;;; 'f8' is float 64
    # mask_right = rgb2grey(imread("IR_0810-dir.png", flatten=True))
    # mask_left = rgb2grey(imread("IR_0810-esq.png", flatten=True))
    # img_left = np.copy(imageMama)
    # img_right = np.copy(imageMama)
    #
    # img_left[mask_left == 0] = 0
    # img_right[mask_right == 0] = 0
    #
    #
    # img_left = rgb2grey(img_left)
    #
    # plt.imshow(imageMama, cmap='gray')
    # plt.show()