import numpy as np
from Core.SDH import SumDiffHistogram
from Core.GLCM import GLCM_Descriptors

from skimage.data import astronaut
from skimage.color import rgb2grey
from skimage.io import imread
from skimage.viewer import ImageViewer

from sklearn.preprocessing import MinMaxScaler
from csv import writer

import matplotlib.pyplot as plt

from timeit import default_timer as timer
class GetProcessingTime():



    def __init__(self, start = False):

        if start:
            image = self.imagesToTest()
            #results = []
            #results.append("Computing Time")
            #results += self.calcStructuresComputingTime(image, 0, 1)  #0
            #results += self.calcStructuresComputingTime(image, 1, 1)  #45
            #results += self.calcStructuresComputingTime(image, 1, 0)  #90
            #results += self.calcStructuresComputingTime(image, 1, -1) #135
            #self.saveCSV("results", results)
            #results = ["0º", "a"]

            results = ["ASM - HSD", "ASM - GLCM", "ASM - X",
                       "ENERGY - HSD", "ENERGY - GLCM", "ENERGY - X",
                       "ENTROPY - HSD", "ENTROPY - GLCM", "ENTROPY - X",
                       "CONTRAST - HSD", "CONTRAST - GLCM", "CONTRAST - X",
                       "DISSIMILARITY - HSD", "DISSIMILARITY - GLCM", "DISSIMILARITY - X",
                       "HOMOGENEITY - HSD", "HOMOGENEITY - GLCM", "HOMOGENEITY - X",
                       "MEAN - HSD", "MEAN - GLCM", "MEAN - X",
                       "CORRELATION - HSD", "CORRELATION - GLCM", "CORRELATION - X",
                       "VARIANCE- HSD", "VARIANCE - GLCM", "VARIANCE - X",
                       "CLUSTER SHADE - HSD", "CLUSTER SHADE  - GLCM", "CLUSTER SHADE  - X",
                       "CLUSTER PROMINENCE - HSD", "CLUSTER PROMINENCE - GLCM", "CLUSTER PROMINENCE - X"]
            self.saveCSV("resultsDescriptors", results)

            results = []
            results += self.calc(image,0,1)
            self.saveCSV("resultsDescriptors", self.calc(image,0,1))
            self.saveCSV("resultsDescriptors", self.calc(image, 1, 1))
            self.saveCSV("resultsDescriptors", self.calc(image, 1, 0))
            self.saveCSV("resultsDescriptors", self.calc(image, 1, 1))

    def imagesToTest(self):
        image1 = np.array([(0, 0, 0, 0), (0, 0, 0, 0), (1, 1, 1, 1), (1, 1, 1, 1)])
        image2 = np.array([(0, 0, 1, 1), (0, 0, 1, 1), (0, 0, 1, 1), (0, 0, 1, 1)])
        image3 = np.array(
            [(0, 0, 0, 0, 0, 0), (0, 0, 0, 0, 0, 0), (1, 1, 1, 1, 1, 1), (1, 1, 1, 1, 1, 1), (2, 2, 2, 2, 2, 2),
             (2, 2, 2, 2, 2, 2)])

        image4 = np.array([(0, 0, 0, 0, 0, 0, 0, 0),
                           (0, 0, 0, 0, 0, 0, 0, 0),
                           (1, 1, 1, 1, 1, 1, 1, 1),
                           (1, 1, 1, 1, 1, 1, 1, 1),
                           (2, 2, 2, 2, 2, 2, 2, 2),
                           (2, 2, 2, 2, 2, 2, 2, 2),
                           (3, 3, 3, 3, 3, 3, 3, 3),
                           (3, 3, 3, 3, 3, 3, 3, 3)])

        image5 = np.array([(0, 1, 2, 3, 0, 1, 2, 3),
                           (0, 1, 2, 3, 0, 1, 2, 3),
                           (0, 1, 2, 3, 0, 1, 2, 3),
                           (0, 1, 2, 3, 0, 1, 2, 3),
                           (0, 1, 2, 3, 0, 1, 2, 3)])

        image6 = np.array([(0, 0, 0, 0, 0, 0, 0, 0),
                           (1, 1, 1, 1, 1, 1, 1, 1),
                           (2, 2, 2, 2, 2, 2, 2, 2),
                           (3, 3, 3, 3, 3, 3, 3, 3)])

        image7 = np.array([(0, 0, 0, 1, 1, 0, 0, 0),
                           (0, 0, 0, 0, 0, 0, 0, 0),
                           (1, 1, 1, 1, 3, 3, 1, 1),
                           (1, 1, 2, 2, 1, 1, 1, 1),
                           (2, 2, 0, 0, 2, 2, 2, 2),
                           (3, 3, 2, 2, 2, 2, 2, 2),
                           (3, 3, 3, 3, 1, 1, 3, 3),
                           (2, 2, 3, 3, 3, 3, 3, 3)])

        scaler = MinMaxScaler(feature_range=(0, 255))
        imageX = rgb2grey(astronaut())
        imageX = scaler.fit_transform(imageX)
        imageX = imageX.astype(int)

        imageTireoide = rgb2grey(imread("imgtireoide.jpg"))
        imageTireoide = scaler.fit_transform(imageTireoide)
        imageTireoide = imageTireoide.astype(int)

        imageMama = rgb2grey(imread("imagMama.PNG"))
        imageMama = scaler.fit_transform(imageMama)
        imageMama = imageMama.astype(int)

        imageMamaDireita = rgb2grey(imread("imgMamaRight.PNG"))
        imageMamaDireita = scaler.fit_transform(imageMamaDireita)
        imageMamaDireita = imageMamaDireita.astype(int)

        image = imageX

        return image

    def calcStructuresComputingTime(self, image, di, dj, border=False):

        #############################################
        ####     Structures Computing  Time      ####
        #############################################

        SDHCalculator = SumDiffHistogram()
        GLCMCalculator = GLCM_Descriptors()
        result = []

        timeHSD = self.getTime(SDHCalculator.prepare_image, (image, di, dj, border))
        timeGLCM = self.getTime(GLCMCalculator.prepare_image, (image, di, dj, border))

        print(timeHSD, timeGLCM)
        print(self.calcDiffBetweenTimes(timeHSD,timeGLCM))
        result.append(timeHSD)
        result.append(timeGLCM)
        result.append(self.calcDiffBetweenTimes(timeHSD,timeGLCM))

        return  result

    def calc(self, image, di, dj, border=False):

        #############################################
        ####     Descriptores          Time      ####
        #############################################

        SDHCalculator = SumDiffHistogram()
        GLCMCalculator = GLCM_Descriptors()
        result = []

        sum_occurrences, diff_occurences = SDHCalculator.prepare_image(image,di,dj,border)
        glcm = GLCMCalculator.prepare_image(image,di,dj,border)

        grey_levelsHSD = SDHCalculator.get_gray_levels(image)
        grey_levelsGLCM = GLCMCalculator.get_gray_levels(image)
        meanHSD = SDHCalculator.calc_MEAN(sum_occurrences, grey_levelsHSD)
        meanGLCM = GLCMCalculator.calc_MEAN(glcm, grey_levelsGLCM)
        asmHSD = SDHCalculator.calc_ASM(sum_occurrences)
        asmGLCM = GLCMCalculator.calc_ASM(glcm)


        #### ASM
        timeHSD = self.getTime(SDHCalculator.calc_ASM, (sum_occurrences,))
        timeGLCM = self.getTime(GLCMCalculator.calc_ASM, (glcm,))
        print(timeHSD, timeGLCM)
        print(self.calcDiffBetweenTimes(timeHSD, timeGLCM))
        result.append(timeHSD)
        result.append(timeGLCM)
        result.append(self.calcDiffBetweenTimes(timeHSD, timeGLCM))


        #### ENERGY
        timeHSD = self.getTime(SDHCalculator.calc_ENERGY, (asmHSD,))
        timeGLCM = self.getTime(GLCMCalculator.calc_ENERGY, (asmGLCM,))
        print(timeHSD, timeGLCM)
        print(self.calcDiffBetweenTimes(timeHSD, timeGLCM))
        result.append(timeHSD)
        result.append(timeGLCM)
        result.append(self.calcDiffBetweenTimes(timeHSD, timeGLCM))

        #### ENTROPY
        timeHSD = self.getTime(SDHCalculator.calc_ENTROPY, (sum_occurrences,))
        timeGLCM = self.getTime(GLCMCalculator.calc_ENTROPY, (glcm,))
        print(timeHSD, timeGLCM)
        print(self.calcDiffBetweenTimes(timeHSD, timeGLCM))
        result.append(timeHSD)
        result.append(timeGLCM)
        result.append(self.calcDiffBetweenTimes(timeHSD, timeGLCM))


        #### CONTRAST
        timeHSD = self.getTime(SDHCalculator.calc_CONTRAST, (diff_occurences,))
        timeGLCM = self.getTime(GLCMCalculator.calc_CONTRAST, (glcm,))
        print(timeHSD, timeGLCM)
        print(self.calcDiffBetweenTimes(timeHSD, timeGLCM))
        result.append(timeHSD)
        result.append(timeGLCM)
        result.append(self.calcDiffBetweenTimes(timeHSD, timeGLCM))

        #### DISSIMILARITY
        timeHSD = self.getTime(SDHCalculator.calc_DISSIMILARITY, (diff_occurences,))
        timeGLCM = self.getTime(GLCMCalculator.calc_DISSIMILARITY, (glcm,))
        print(timeHSD, timeGLCM)
        print(self.calcDiffBetweenTimes(timeHSD, timeGLCM))
        result.append(timeHSD)
        result.append(timeGLCM)
        result.append(self.calcDiffBetweenTimes(timeHSD, timeGLCM))

        #### HOMOGENEITY
        timeHSD = self.getTime(SDHCalculator.calc_HOMOGENEITY, (diff_occurences,))
        timeGLCM = self.getTime(GLCMCalculator.calc_HOMOGENEITY, (glcm,))
        print(timeHSD, timeGLCM)
        print(self.calcDiffBetweenTimes(timeHSD, timeGLCM))
        result.append(timeHSD)
        result.append(timeGLCM)
        result.append(self.calcDiffBetweenTimes(timeHSD, timeGLCM))

        #### MEAN
        timeHSD = self.getTime(SDHCalculator.calc_MEAN, (sum_occurrences, grey_levelsHSD))
        timeGLCM = self.getTime(GLCMCalculator.calc_MEAN, (glcm, grey_levelsGLCM))
        print(timeHSD, timeGLCM)
        print(self.calcDiffBetweenTimes(timeHSD, timeGLCM))
        result.append(timeHSD)
        result.append(timeGLCM)
        result.append(self.calcDiffBetweenTimes(timeHSD, timeGLCM))


        #### CORRELATION
        timeHSD = self.getTime(SDHCalculator.calc_CORRELATION, (sum_occurrences, diff_occurences, meanHSD))
        timeGLCM = self.getTime(GLCMCalculator.calc_CORRELATION, (glcm, meanGLCM))
        print(timeHSD, timeGLCM)
        print(self.calcDiffBetweenTimes(timeHSD, timeGLCM))
        result.append(timeHSD)
        result.append(timeGLCM)
        result.append(self.calcDiffBetweenTimes(timeHSD, timeGLCM))

        #### VARIANCE
        timeHSD = self.getTime(SDHCalculator.calc_VARIANCE, (sum_occurrences, diff_occurences, meanHSD))
        timeGLCM = self.getTime(GLCMCalculator.calc_VARIANCE, (glcm, meanGLCM))
        print(timeHSD, timeGLCM)
        print(self.calcDiffBetweenTimes(timeHSD, timeGLCM))
        result.append(timeHSD)
        result.append(timeGLCM)
        result.append(self.calcDiffBetweenTimes(timeHSD, timeGLCM))

        #### CLUSTER_SHADE
        timeHSD = self.getTime(SDHCalculator.calc_CLUSTER_SHADE, (sum_occurrences, meanHSD))
        timeGLCM = self.getTime(GLCMCalculator.calc_CLUSTER_SHADE, (glcm, meanGLCM))
        print(timeHSD, timeGLCM)
        print(self.calcDiffBetweenTimes(timeHSD, timeGLCM))
        result.append(timeHSD)
        result.append(timeGLCM)
        result.append(self.calcDiffBetweenTimes(timeHSD, timeGLCM))

        #### CLUSTER_PROMINENCE
        timeHSD = self.getTime(SDHCalculator.calc_CLUSTER_PROMINENCE, (sum_occurrences, meanHSD))
        timeGLCM = self.getTime(GLCMCalculator.calc_CLUSTER_PROMINENCE, (glcm, meanGLCM))
        print(timeHSD, timeGLCM)
        print(self.calcDiffBetweenTimes(timeHSD, timeGLCM))
        result.append(timeHSD)
        result.append(timeGLCM)
        result.append(self.calcDiffBetweenTimes(timeHSD, timeGLCM))


        '''
        asm = calculator.calc_ASM(sum_occurences)  # self.calc_Total_ASM(sum_occurences,diff_occurences)#
        energy = calculator.calc_ENERGY(asm)
        entropy = calculator.calc_ENTROPY(sum_occurences)
        contrast = calculator.calc_CONTRAST(diff_occurences)
        dissimilarity = calculator.calc_DISSIMILARITY(diff_occurences)
        homogeneity = calculator.calc_HOMOGENEITY(diff_occurences)
        mean = calculator.calc_MEAN(sum_occurences, calculator.get_gray_levels(image))
        correlation = calculator.calc_CORRELATION(sum_occurences, diff_occurences, mean)
        print("Time using python methods: ", end - start)
        variance = calculator.calc_VARIANCE(sum_occurences, diff_occurences, mean)
        cluster_shade = calculator.calc_CLUSTER_SHADE(sum_occurences, mean)
        cluster_prominence = calculator.calc_CLUSTER_PROMINENCE(sum_occurences, mean)
        # std = self.calc_STD(sum_occurences, diff_occurences)



        print("\nSHD\nASM:", asm)
        print("ENERGY", energy)
        print("ENTROPY", entropy)
        print("CONTRAST:", contrast)
        print("DISSIMILARITY", dissimilarity)
        print("HOMOGENEITY:", homogeneity)
        print("MEAN:", mean)
        print("CORRELATION:", correlation)
        print("VARIANCE:", variance)
        print("CLUSTER SHADE:", cluster_shade)
        print("CLUSTER PROMINENCE:", cluster_prominence)
        '''
        return  result

    def getTime(self, descriptorFunction, parameters):
        start = timer()
        descriptorFunction(*parameters)
        end = timer()
        return end - start

    def calcDiffBetweenTimes(self, timeX, timeY):
        'TO DO: Refatorar para gerar precisão decimal na diferença '
        cont = 0
        if timeX < timeY:
            smallerTime = timeX
            biggerTimer = timeY
        else:
            smallerTime = timeY
            biggerTimer = timeX

        aux = smallerTime
        while (aux <= biggerTimer):
            cont +=1
            aux = smallerTime * cont
        return cont

    def saveCSV(self, arq, features):
        with open(arq, 'a') as csvfile:
            temp = writer(csvfile, delimiter=';', dialect='excel')
            temp.writerow(features)


if __name__ == "__main__":
    program = GetProcessingTime()




    # imageMama =   np.loadtxt("IR_0810.txt")#, dtype='f8') # dtype='f3' is float 32 ;;; 'f8' is float 64
    # mask_right = rgb2grey(imread("IR_0810-dir.png", flatten=True))
    # mask_left = rgb2grey(imread("IR_0810-esq.png", flatten=True))
    # img_left = np.copy(imageMama)
    # img_right = np.copy(imageMama)
    #
    # img_left[mask_left == 0] = 0
    # img_right[mask_right == 0] = 0
    #
    #
    # img_left = rgb2grey(img_left)
    #
    # plt.imshow(imageMama, cmap='gray')
    # plt.show()