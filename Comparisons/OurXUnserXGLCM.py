from Core.SDH import SumDiffHistogram
from Core.SDH_Unser import UnserSumDiffHistogram
from Core.GLCM import GLCM_Descriptors
from Utils.images import imagesToTest, syntheticImagesToPaper2, brodazImagesToPaper2, regularGeometricImages, syntheticImagesToPaper3, ponceImagesToPaper3
from Utils.datasets import normalizeDataset
from Comparisons.Graphics.Common import GraphicsCore
import matplotlib.pyplot as plt
from skimage import io
from operator import add
import numpy as np

from Utils.MasterThesisImages import *



class GraphComparison:

    def __init__(self):

        print(
            "asm, entropy, energy, homogeneity, correlationS, dissimilarity, contrast, meanI, meanJ,global_mean, correlation, variance, stdJ, cluster_shade, cluster_prominence\n")
        #image = imagesToTest()
        #images = syntheticImagesToPaper3()
        #images = ponceImagesToPaper3("../StaticFiles/Ponce/paper3/ponce/")
        #images = brodazImagesToPaper2("../StaticFiles/Brodatz/paper2/brodatz/")
        #images = [images[1]]
        images = syntheticImages()
        #images = ponceImages("C:\\Users\Aluno\Documents\\UFF\Projetos\[UFF-MEST] GLCM x SDH\StaticFiles\Ponce\Dissertação_v2\\textures\\")

        #images = [images[0]]
        for image in images:
            plt.imshow(image,  cmap='gray')
            plt.axis('off')
            io.imshow(image, cmap="gray")
            #
            plt.show()

            showImage(image,"")
            Our_SDH, Unser_SDH, GLCM_Results= [], [] ,[]
            di = 0
            dj = 1
            #uXg_values, oXg_values = np.array(), np.array()
            Our_SDH, Unser_SDH, GLCM_Results= self.get_Results(image, di, dj)
            #GraphicsCore(Our_SDH, Unser_SDH, GLCM_Results)
            #uXg_values, oXg_values = self.ourDifferenceValuesIsSmaller(Our_SDH.features, Unser_SDH.features, GLCM_Results.features)

            di = 1
            dj = 1

            Our_SDH, Unser_SDH, GLCM_Results = self.get_Results(image, di, dj)
            #GraphicsCore(Our_SDH, Unser_SDH, GLCM_Results)
            #u, o = self.ourDifferenceValuesIsSmaller(Our_SDH.features, Unser_SDH.features, GLCM_Results.features)
            #uXg_values =  uXg_values + u
            #oXg_values = oXg_values + o


            di = 1
            dj = 0

            Our_SDH, Unser_SDH, GLCM_Results = self.get_Results(image, di, dj)
            #GraphicsCore(Our_SDH, Unser_SDH, GLCM_Results)
            #u, o = self.ourDifferenceValuesIsSmaller(Our_SDH.features, Unser_SDH.features, GLCM_Results.features)
          #  uXg_values =  uXg_values + u
            #oXg_values = oXg_values + o

            di = 1
            dj = -1

            Our_SDH, Unser_SDH, GLCM_Results= self.get_Results(image, di, dj)
            #GraphicsCore(Our_SDH, Unser_SDH, GLCM_Results)
            #u, o = self.ourDifferenceValuesIsSmaller(Our_SDH.features, Unser_SDH.features, GLCM_Results.features)
            #uXg_values =  uXg_values + u
            #oXg_values = oXg_values + o


            print("\n*******************************************************************\n")
           #  #print("uxggg", uXg_values, oXg_values)
           #  uXg_values = uXg_values / 4
           #  oXg_values = oXg_values / 4
           #
           #
           #  uXg_values = list(uXg_values)
           #  oXg_values = list(oXg_values)
           #  u, o = [],[]
           #  for i in range(len(uXg_values)):
           #      uXg = uXg_values[i]
           #      oXg = oXg_values[i]
           #      if type(uXg) is not str and type(uXg) is not np.str_: uXg = ("{:.5f}".format(uXg))
           #      if type(oXg) is not str and type(oXg) is not np.str_: oXg = ("{:.5f}".format(oXg))
           #
           #      if uXg == 0: uXg = "No Diff."
           #      if oXg == 0: oXg = "No Diff."
           #      u.append(uXg)
           #      o.append(oXg)
           #
           #  print("DIFF APROX:\nUxG", u, "\nOxG", o)
           #
           # # GraphicsCore(o, u, [])
           #
           #  print("\n####################################################################")

        print("Done!")

    def get_Results(self, image, di, dj):

        rjd = False
        Our_SDH, Unser_SDH, GLCM_Results = [], [], []
        GLCM_Results = GLCM_Descriptors(image, di, dj, returnJustDifferences=rjd)
        Our_SDH = SumDiffHistogram(image, di, dj, returnJustDifferences=rjd)
        Unser_SDH = UnserSumDiffHistogram(image, di, dj, returnJustDifferences=rjd)





        return Our_SDH, Unser_SDH, GLCM_Results

    def ourDifferenceValuesIsSmaller(self, our, unser, glcm):

        oXg_values = []
        uXg_values = []
        oXu_values = []
        oXu_logic = []
        for i in range(len(our)):
            o = our[i]
            u = unser[i]
            g = glcm[i]

            if o > g:
                r_OxG = o - g
            else:
                r_OxG = g - o


            if type(u) is str:
                r_UxG = 0
                print("type", type(u))
                oXu_logic.append(u)

            else:
                if u > g:
                    r_UxG = u - g
                else:
                    r_UxG = g - u

                oXu_logic.append(r_OxG <= r_UxG)
                if r_UxG > r_OxG: oXu_values.append(r_UxG - r_OxG)
                else: oXu_values.append(r_OxG - r_UxG)

            oXg_values.append(r_OxG)
            uXg_values.append(r_UxG)



        #print("U",uXg_values, "\nO", oXg_values,"\nR", oXu_logic)
        return np.array(uXg_values), np.array(oXg_values)
        #return uXg_values, oXg_values


if __name__ == "__main__":
    program = GraphComparison()
