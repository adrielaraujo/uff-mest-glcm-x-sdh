import numpy as np
import csv

def normalizeDataset(dataset_dir, justSave=False):
    X,y = separeteClasses(dataset_dir)

    X = X/np.sum(X)

    dataset = np.insert(X, X.shape[1], y, axis=1)

    if justSave:
        saveCSV(dataset_dir.replace(".csv", "_Normalized.csv"), dataset)
        return
    else:
        return dataset


def saveCSV(arq, features):
    with open(arq, 'a') as csvfile:
        temp = csv.writer(csvfile, delimiter=';', dialect='excel')
        temp.writerow(features)

def separeteClasses(dataset_file):

    if type(dataset_file) is str:
        dataset = np.loadtxt(dataset_file, delimiter=";")
    else:
        dataset = dataset_file

    X = dataset[::,:-1]
    y = dataset[::,-1]

    return X,y
