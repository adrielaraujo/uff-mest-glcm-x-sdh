import numpy as np
#from scipy.ndimage.interpolation import rotate
from skimage.transform import rotate
from Utils.images import saveImage, saveImages, showImage


from skimage.io import imread, imsave
import os


all_textons = []

def syntheticImages():
    return horizontalStripes()

def horizontalStripes():
    sizeTextures = 128

    texton1 = np.array(
        [
            [0],
            # [85],
            # [170],
            [255]
        ]
    )

    texton2 = np.array(
        [
            [0,0],
            [0,0],
            # [85, 85],
            # [85, 85],
            # [170, 170],
            # [170, 170],
            [255,255],
            [255,255]
        ]
    )

    texton3 = np.array(
        [
            [0,0,0,0],
            [0,0,0,0],
            [0,0,0,0],
            [0,0,0,0],
            # [85, 85, 85, 85],
            # [85, 85, 85, 85],
            # [85, 85, 85, 85],
            # [85, 85, 85, 85],
            # [170, 170, 170, 170],
            # [170, 170, 170, 170],
            # [170, 170, 170, 170],
            # [170, 170, 170, 170],
            [255,255,255,255],
            [255,255,255,255],
            [255,255,255,255],
            [255,255,255,255]
        ]
    )


    texton4 = np.array(range(0,255, 64)) #4 tons
    texton5 = np.array(range(0, 255, 8))  # 32 tons
    texton6 = np.array(range(0, 255, 2)) #128 tons

    texton7 = expandeTexton(texton4, 2)
    texton8 = expandeTexton(texton4, 4)

    texton9 = expandeTexton(texton5, 2)
    texton10 = expandeTexton(texton5, 4)

    texton11 = expandeTexton(texton6, 2)
    texton12 = expandeTexton(texton6, 4)

    randon_texton = np.array(
        [
            [144, 64, 160, 112, 96, 32, 240, 176],
            [80, 208, 224, 48, 16, 80, 240, 0],
            [128, 0, 208, 160, 112, 192, 144, 128],
            [192, 96, 224, 208, 80, 128, 160, 64],
            [176, 240, 32, 192, 0, 224, 192, 80],
            [96, 80, 224, 16, 48, 0, 32, 96],
            [112, 16, 64, 48, 192, 160, 64, 0],
            [144, 32, 224, 224, 176, 64, 208, 32]
         ]
     )
    randon_texton2 = expandeTexton(randon_texton, 2)
    randon_texton3 = expandeTexton(randon_texton, 4)


    binary_boards_textons = (texton1,)#,texton2, texton3)
    inverted_stripes_textons = (texton4, texton5, texton6,texton7,texton8,texton9,texton10,texton11,texton12)
    randon_textons = (randon_texton, randon_texton2, randon_texton3)

    all_textons = binary_boards_textons + inverted_stripes_textons + randon_textons#[texton1, texton2, texton3, texton4, texton5, texton6, randon_texton]
#    saveImages(all_textons, "StaticFiles/Synthetic Images Dataset/Textons")

    #showImage(texton4)

    #anglesRotations = (0, 11.25, 22.5, 33.75, 45, 56.25, 67.5, 78.75, 90, 101.25, 112.50, 123.75, 135)
    anglesRotations = (0, 45, 90, 135, 11.25, 22.5, 33.75, 56.25, 67.5, 78.75,101.25, 112.50, 123.75)

    #anglesRotations = {45}
    #images = makeHorizontalStripes(binary_boards_textons, sizeTextures)
    images = makeRotation(makeHorizontalStripes(binary_boards_textons, sizeTextures), anglesRotations)

    #images += makeRandomBoxes(randon_textons, sizeTextures)
    #images += makeRandomBoxes(randon_textons, sizeTextures, multiply_factor=2)
    #images += makeRandomBoxes(randon_textons, sizeTextures, multiply_factor=4)
    #
    #images = makeInvertedStripes(inverted_stripes_textons, sizeTextures) #cada box com 1 pixels
    #images += makeInvertedStripes(inverted_stripes_textons, sizeTextures, multiply_factor=2) # cada box com 1x2 pixels
    #images += makeInvertedStripes(inverted_stripes_textons, sizeTextures, multiply_factor=4) #cada box com 1x4 pixels

    #images += makeBoards(binary_boards_textons, sizeTextures)


    print("finish")
    return  images


def makeBoards(textons, sizeTextures):

    boards = []
    for texton in textons:
        texton_reflected = np.concatenate((texton, texton[::-1]), 1)
        texture = np.tile(texton_reflected,(sizeTextures / texton_reflected.shape[0], sizeTextures / texton_reflected.shape[1]))
        boards.append(texture)

    return boards

def makeHorizontalStripes(textons, sizeTextures, orientation = "horizontal"):

    if type(sizeTextures) == int: sizeTextures = (sizeTextures,)
    stripes = []
    for size in sizeTextures:
        for texton in textons:
            texture = np.tile(texton, (int(size / texton.shape[0]), int(size / texton.shape[1])))
            stripes.append(texture)

            #showImage( texture)
            #print(texture)

    return stripes

def makeInvertedStripes(textons, sizeTextures, orientation = "horizontal", multiply_factor = None):
    stripes = []
    for texton in textons:

        texton_reflected =  texton

        if(len(texton.shape) == 1):
            texton_reflected = np.append([texton], [texton[::-1]], axis=0)

        if multiply_factor:
            texton_reflected = np.repeat(texton_reflected, multiply_factor, axis=0)
            texton_reflected = np.repeat(texton_reflected, multiply_factor, axis=1)

        if max(texton.shape) <= sizeTextures:
            #print("texton shape", texton_reflected.shape)
            lines = sizeTextures / texton_reflected.shape[0]
            cols = sizeTextures / texton_reflected.shape[1]

            texture = np.tile(texton_reflected, (lines, cols))
            #print(texton_reflected.shape) #SHAPES DOS TEXTONS

            stripes.append(texture)

    return stripes

def makeRandomBoxes(textons, sizeTextures, multiply_factor = None):
    randon_boxes = []

    for texton in textons:
        if multiply_factor:
            texton = np.repeat(np.repeat(texton, multiply_factor, axis=0), multiply_factor, axis=1)

        texture = np.tile(texton, (sizeTextures / texton.shape[0], sizeTextures / texton.shape[1]))
        randon_boxes.append(texture)

    return randon_boxes

def makeRotation(textures, angles):


    images = []
    for angle in angles:
        for texture in textures:
            image = rotate(texture, angle,  preserve_range=True, mode='wrap',  clip=False)
            image[image < 127] = 0
            image[image >=127] = 255
            images.append(image)
            #print(image)
            #showImage(image)



    # textureaux = images[-1]
    # textureaux = np.tile(textureaux, 2)
    #
    # showImage(images[-1])
    # showImage(textureaux)

    return images

def expandeTexton(texton, multiply_factor, is_stripe=False):
    if len(texton.shape) == 2:
        return np.repeat(np.repeat(texton, multiply_factor, axis=0), multiply_factor, axis=1)
    else:
        texton_reflected = np.append([texton], [texton[::-1]], axis=0)
        return np.repeat(np.repeat(texton_reflected, multiply_factor, axis=0), multiply_factor, axis=1)

def imagesToDissertation():
    texton3 = np.array(
        [
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            # [85, 85, 85, 85],
            # [85, 85, 85, 85],
            # [85, 85, 85, 85],
            # [85, 85, 85, 85],
            # [170, 170, 170, 170],
            # [170, 170, 170, 170],
            # [170, 170, 170, 170],
            # [170, 170, 170, 170],
            [255, 255, 255, 255],
            [255, 255, 255, 255],
            [255, 255, 255, 255],
            [255, 255, 255, 255]
        ]
    )
    textons = [texton3]
    sizes = [512,256,128]
    textures = makeHorizontalStripes(textons, sizes)

    anglesRotations = (0, 11.25, 22.5, 33.75, 45, 56.25, 67.5, 78.75, 90, 101.25, 112.50, 123.75, 135)
    images = makeRotation(textures, anglesRotations)

    # for index, texture in enumerate(images):
    #     showImage(texture)
    #     saveImage("..\StaticFiles\Synthetic Images Dataset\ImagesToDissertation\Image " + str(index) + ".jpg",texture)
    #
    # saveImages( images, "..\StaticFiles\Synthetic Images Dataset\ImagesToDissertation")

def ponceImages(dir= None):
    if dir:
        packages = ["brick1/", "cordinary/", "brick1/"]
        images = []
        for pack in packages:
            full_dir = dir + pack
            print("FULL DIR", full_dir)
            for file in os.listdir(full_dir):
                images.append(imread(full_dir + file))
        return images


# imagesToDissertation();