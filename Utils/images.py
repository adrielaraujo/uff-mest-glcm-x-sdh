from skimage.data import astronaut, coffee, clock
from skimage.color import rgb2grey
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
from skimage import io

from skimage.io import imread, imsave

import numpy as np
import os, csv
import random


def imagesToTest():


    #image1 = np.array([(0, 0, 0, 0), (0, 0, 0, 0), (1, 1, 1, 1), (1, 1, 1, 1)])

    #image2 = np.array([(0, 0, 1, 1), (0, 0, 1, 1), (0, 0, 1, 1), (0, 0, 1, 1)])


    image3 = np.array(
        [(0, 0, 0, 0, 0, 0), (0, 0, 0, 0, 0, 0), (1, 1, 1, 1, 1, 1), (1, 1, 1, 1, 1, 1), (2, 2, 2, 2, 2, 2),
         (2, 2, 2, 2, 2, 2)])


    image4 = np.array([(0, 0, 0, 0, 0, 0, 0, 0),
                       (0, 0, 0, 0, 0, 0, 0, 0),
                       (1, 1, 1, 1, 1, 1, 1, 1),
                       (1, 1, 1, 1, 1, 1, 1, 1),
                       (2, 2, 2, 2, 2, 2, 2, 2),
                       (2, 2, 2, 2, 2, 2, 2, 2),
                       (3, 3, 3, 3, 3, 3, 3, 3),
                       (3, 3, 3, 3, 3, 3, 3, 3)])

    image5 = np.array([(0, 1, 2, 3, 0, 1, 2, 3),
                       (0, 1, 2, 3, 0, 1, 2, 3),
                       (0, 1, 2, 3, 0, 1, 2, 3),
                       (0, 1, 2, 3, 0, 1, 2, 3),
                       (0, 1, 2, 3, 0, 1, 2, 3)])

    image6 = np.array([(0, 0, 0, 0, 0, 0, 0, 0),
                       (1, 1, 1, 1, 1, 1, 1, 1),
                       (2, 2, 2, 2, 2, 2, 2, 2),
                       (3, 3, 3, 3, 3, 3, 3, 3)])

    image7 = np.array([(0, 0, 0, 1, 1, 0, 0, 0),
                       (0, 0, 0, 0, 0, 0, 0, 0),
                       (1, 1, 1, 1, 3, 3, 1, 1),
                       (1, 1, 2, 2, 1, 1, 1, 1),
                       (2, 2, 0, 0, 2, 2, 2, 2),
                       (3, 3, 2, 2, 2, 2, 2, 2),
                       (3, 3, 3, 3, 1, 1, 3, 3),
                       (2, 2, 3, 3, 3, 3, 3, 3)])

    image8 = np.array(
        [(1, 0, 1, 0, 2, 0), (0, 1, 1, 1, 0, 2), (0, 1, 1, 1, 0, 1), (1, 1, 0, 0, 2, 2), (2, 0, 2, 2, 0, 2),
         (1, 2, 1, 2, 0, 2)])

    image9 = np.array([(0, 0, 0, 0, 0, 0, 0, 0),
                       (0, 0, 0, 0, 0, 0, 0, 0),
                       (1, 1, 1, 1, 1, 1, 1, 1),
                       (1, 1, 1, 1, 1, 1, 1, 1),
                       (0, 0, 0, 0, 0, 0, 0, 0),
                       (0, 0, 0, 0, 0, 0, 0, 0),
                       (1, 1, 1, 1, 1, 1, 1, 1),
                       (1, 1, 1, 1, 1, 1, 1, 1)])

    scaler = MinMaxScaler(feature_range=(0, 255))
    imageX = rgb2grey(astronaut())
    imageX = scaler.fit_transform(imageX)
    imageX = imageX.astype(int)

    imageCoffe = rgb2grey(coffee())
    imageCoffe = scaler.fit_transform(imageCoffe)
    imageCoffe = imageCoffe.astype(int)

    imageClock = rgb2grey(clock())
    imageClock = scaler.fit_transform(imageClock)
    imageClock = imageClock.astype(int)


    imageTireoide = rgb2grey(imread("C:\\Users\Aluno\Documents\\UFF\Projetos\[UFF-MEST] GLCM x SDH\StaticFiles\imgtireoide.jpg"))
    imageTireoide = scaler.fit_transform(imageTireoide)
    imageTireoide = imageTireoide.astype(int)
    print(imageTireoide.max())


    imageMama = rgb2grey(imread("C:\\Users\Aluno\Documents\\UFF\Projetos\[UFF-MEST] GLCM x SDH\StaticFiles\imagMama.PNG"))
    imageMama = scaler.fit_transform(imageMama)
    imageMama = imageMama.astype(int)

    imageMamaDireita = rgb2grey(imread("C:\\Users\Aluno\Documents\\UFF\Projetos\[UFF-MEST] GLCM x SDH\StaticFiles\imgMamaRight.PNG"))
    imageMamaDireita = scaler.fit_transform(imageMamaDireita)
    imageMamaDireita = imageMamaDireita.astype(int)


    image = image3
    images = [image3,image4,image5,image6,image7,image8,imageX, imageClock, imageCoffe]

    out = [imageMama, imageMamaDireita, imageTireoide]
    return [image9]

def convertToAngle(di, dj):
    if abs(di) < abs(dj):
        level = 0
    elif abs(di) > abs(dj):
        level = 90
    elif di == dj and dj > 0:
        level = 45
    else:
        level = 135
    return  level

def readImagesFromDir(dir, extension=".jpg"):

    images = []
    for file in os.listdir(dir):
        images.append(imread(dir+file))
    return images

def splitIntoQuadrants(image):

    x, y = image.shape
    s = 2

    a1 = image[:x//s, :y//s]
    a2 = image[:x//s, x//s:]
    a3 = image[x//s:, :x//s]
    a4 = image[x//s:, x//s:]

    return a1,a2,a3,a4

def syntheticImagesToPaper1(orientation = "horizontal"):

    images = []
    if orientation == "horizontal":
        images.append(
            np.array([(0, 0, 0, 0, 0, 0, 0, 0),
                      (85,85,85,85,85,85,85,85),
                      (170,170,170,170,170,170,170,170),
                     # (200,200,200,200,200,200,200,200),
                     # (220,220,220,220,220,220,220,220),
                      (255,255,255,255,255,255,255,255),
                      (0, 0, 0, 0, 0, 0, 0, 0),
                      (85, 85, 85, 85, 85, 85, 85, 85),
                      (170, 170, 170, 170, 170, 170, 170, 170),
                      #(200, 200, 200, 200, 200, 200, 200, 200),
                     # (220, 220, 220, 220, 220, 220, 220, 220),
                      (255, 255, 255, 255, 255, 255, 255, 255)])
        )

        images.append(
            np.array([(0, 36, 72, 108, 144, 180, 216, 255),
                      (0, 36, 72, 108, 144, 180, 216, 255),
                      (0, 36, 72, 108, 144, 180, 216, 255),
                      (0, 36, 72, 108, 144, 180, 216, 255),
                      (0, 36, 72, 108, 144, 180, 216, 255),
                      (0, 36, 72, 108, 144, 180, 216, 255),
                      (0, 36, 72, 108, 144, 180, 216, 255),
                      (0, 36, 72, 108, 144, 180, 216, 255)])
        )

        images.append(
            np.array([[0, 8, 16, 24, 32, 40, 48, 56, 64, 72, 80,
                       88, 96, 104, 112, 120, 128, 136, 144, 152,
                       160, 168, 176,184, 192, 200,208, 216, 224,
                       232, 240, 248]] *32)
        )




    return images

def syntheticImagesToPaper2(orientation = "horizontal"):
    images = []

    if orientation == "horizontal":
        values32 = [0, 8, 16, 24, 32, 40, 48, 56, 64, 72, 80,
                       88, 96, 104, 112, 120, 128, 136, 144, 152,
                       160, 168, 176,184, 192, 200,208, 216, 224,
                       232, 240, 248]
        values16 = [0, 16, 32, 48, 64, 80, 96, 112, 128, 144,
                  160, 176, 192, 208, 224, 240]

        values8 = [0, 36, 72, 108, 144, 180, 216, 255, 0, 36, 72, 108, 144, 180, 216, 255]

        values16d= [0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240,
                    0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240]

        images.append(
            np.array([[0, 36, 72, 108, 144, 180, 216, 255],
                     [36, 72, 108, 144, 180, 216, 255, 0],
                     [72, 108, 144, 180, 216, 255, 0, 36],
                     [108, 144, 180, 216, 255, 0, 36, 72],
                     [144, 180, 216, 255, 0, 36, 72, 108],
                     [180, 216, 255, 0, 36, 72, 108, 144],
                     [216, 255, 0, 36, 72, 108, 144, 180],
                     [255, 0, 36, 72, 108, 144, 180, 216]])
        )

        # images.append(
        #     np.array([[0, 36, 72, 108, 144, 180, 216, 255],
        #               [255, 0, 36, 72, 108, 144, 180, 216],
        #               [216, 255, 0, 36, 72, 108, 144, 180],
        #               [180, 216, 255, 0, 36, 72, 108, 144],
        #               [144, 180, 216, 255, 0, 36, 72, 108],
        #               [108, 144, 180, 216, 255, 0, 36, 72],
        #               [72, 108, 144, 180, 216, 255, 0, 36],
        #               [36, 72, 108, 144, 180, 216, 255, 0]])
        # )

        # images.append(
        #     np.array([values8] * 16).T
        # )
        images.append(
            #np.random.choice(values16, (16,16))
            np.array([[208,  64, 208,  16,  96,  16, 240,  32, 128, 224, 160, 160, 144, 160,  80, 224],
                       [ 80, 128, 128,  80, 128, 144, 128, 176, 160, 208, 128,  48, 208, 224, 240,  64],
                       [ 96, 224,  16,  48,  64,   0,  96, 240,  16,  32, 192,  16,  64, 224,   0,  32],
                       [240, 112, 160,  48,  64, 192,  96, 240, 240,  48, 128, 176,  64, 128,  64, 224],
                       [240,  64, 224,  80, 192,  96, 160, 240,  96,  96,  64, 128,  64, 160,  48, 224],
                       [ 96,  64,  32, 144, 240,  16, 144, 144, 224,  16, 192,  16, 176, 112, 176,  16],
                       [  0,  80, 144,  32,  64,  80, 208, 224,  80, 224, 208, 192, 112, 208, 160,  96],
                       [208, 176, 208,  48, 112,   0, 240,  48, 112, 128,  96,  32,  16, 144,   0,  32],
                       [144, 240, 112, 160,  64,  48, 192,  48, 224,   0, 240, 160, 240, 80, 112,  48],
                       [176,  64,   0,  64,  96,  48, 112,  48, 192, 112, 176, 160, 144, 0,  96, 208],
                       [  0, 144,   0,  16, 208,  48, 176, 176, 144,  48, 176,  16, 240, 96,  64,  32],
                       [ 16, 192,  32, 240, 144, 192,  64, 208,  32,  80,  32, 224,  32, 208, 144,  32],
                       [ 32,   0,  16, 192,  96, 128,  16, 176, 144,  16, 160, 144, 192, 16, 144, 208],
                       [  0, 128, 224,  80, 224,  16,  64, 144, 192, 144,  32, 192, 192, 16,  48,  48],
                       [208, 144, 192, 144, 208,  32,   0,   0,  96, 192,  48, 240, 224, 128,  64,  64],
                       [ 32,  16, 224,  16, 176, 144,  80,  96, 112, 192, 160, 240,  48, 112, 224, 128]])
        )

        # images.append(
        #     np.array([[0, 8, 16, 24, 32, 40, 48, 56, 64, 72, 80,
        #                88, 96, 104, 112, 120, 128, 136, 144, 152,
        #                160, 168, 176, 184, 192, 200, 208, 216, 224,
        #                232, 240, 248]] * 32)
        # )




        # images.append(
        #     np.array([[random.choice(values8) for i in range(16)]] * 16)
        # )





    return images

def syntheticImagesToPaper3():
    images = []


    randon_texton = np.array([[ 96, 128,  40, 184, 232,  72,  80,  128],
       [ 48, 216,  88,  64,  16,  64, 112,  216],
       [208, 184,  56, 168,  16, 120, 232, 184],
       [120,  16, 240, 192,  32,  24, 104, 16],
       [200, 168,  32,   0, 168, 248,  24, 168],
       [144, 112, 208, 176,  72, 232,  56, 112],
       [200,  32,  64, 224, 240,  88, 168, 32],
        [48, 216, 88, 64, 16, 64, 112, 216]])

    chevron_texton = np.array(
        [
            [0, 85, 170, 255, 0, 85, 170, 255],
            [85, 170, 255, 0, 85, 170, 255, 0],
            [170, 255, 0, 85, 170, 255, 0, 85],
            [255, 0, 85, 170, 255, 0, 85, 170],
            [0, 85, 170, 255, 0, 85, 170, 255],
            [255, 0, 85, 170, 255, 0, 85, 170],
            [170, 255, 0, 85, 170, 255, 0, 85],
            [85, 170, 255, 0, 85, 170, 255, 0]

        ]
    )

    mosaic_texton = np.array(
        [
            [255, 255, 255, 255, 255, 255, 255, 255],
            [255, 0, 0, 0, 0, 0, 0, 255],
            [255, 0, 170, 170, 170, 170, 0, 255],
            [255, 0, 170, 85, 85, 170, 0, 255],
            [255, 0, 170, 85, 85, 170, 0, 255],
            [255, 0, 170, 170, 170, 170, 0, 255],
            [255, 0, 0, 0, 0, 0, 0, 255],
            [255, 255, 255, 255, 255, 255, 255, 255],

        ]
    )


    #chevron_texton = np.repeat(chevron_texton,8, axis=1)
    #chevron_texton = np.tile(chevron_texton, (8,1))

    #images.append(mosaic_texton)
    # images.append(np.tile(mosaic_texton, (3, 4)))  #returns a 32x24 img
    images.append(np.tile(mosaic_texton, (6, 8)))  #returns a 64x48 img
    images.append(np.tile(mosaic_texton, (60,80)))  #returns a 640x480 img


    #images.append(chevron_texton)
    #images.append(np.tile(chevron_texton, (3,4)))  #returns a 32x32 img
    images.append(np.tile(chevron_texton, (6, 8)))  #returns a 64x48 img
    images.append(np.tile(chevron_texton, (60, 80))) #returns a 640x480 img

    #images.append(randon_texton)
    # images.append(np.tile(randon_texton, (3, 4))) #returns a 32x32 img
    images.append(np.tile(randon_texton, (6, 8))) #returns a 64x64 img
    images.append(np.tile(randon_texton,(60,80))) #returns a 640x480 img


    return images

def brodazImagesToPaper1(dir = None):
    if dir is None:
      dir = "../StaticFiles/brodaz-2/"

    images = []
    for file in os.listdir(dir):
        images.append(imread(dir + file))
    return images

def brodazImagesToPaper2(dir = None):
    if dir is None:
      dir = "../StaticFiles/paper2/brodatz/"

    images = []
    for file in os.listdir(dir):
        images.append(imread(dir + file))
    return images

def ponceImagesToPaper3 (dir = None):
    if dir is None:
        dir = "../StaticFiles/paper3/ponce/"

    images = []
    for file in os.listdir(dir):
        images.append(imread(dir + file))
    return images

def regularGeometricImages():

    def make_stripes(config):
        for key, values_set in config.items():

            for values in values_set:

                multiply_factor = int(key/len(values))

                if multiply_factor > 1: images.append(np.array([values * multiply_factor] * key))

    def make_boards(config):
        for key, values_set in config.items():

            for values in values_set:

                multiply_factor = int(key/len(values))


                aux = values
                aux.insert(0,aux.pop())

                if multiply_factor > 1:

                    images.append(np.array([aux * multiply_factor] * key))


    images = []

    values2 =   [0, 255]
    values4 =   [0, 85, 170, 255]
    values8 =   [0, 36, 72, 108, 144, 180, 216, 255]
    values16 =  [0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240]
    values32 =  [0, 8, 16, 24, 32, 40, 48, 56, 64, 72, 80, 88, 96, 104, 112, 120, 128, 136, 144, 152, 160, 168, 176, 184, 192, 200, 208, 216, 224, 232, 240, 248]
    values64 =  list(range(0, 255, 4))
    values128 = list(range(0, 255, 2))
    values256 = list(range(0, 255))

    configBoards = {
        8: (values2, values4, values8, values16),
        16: (values2, values4, values8, values16),
        32: (values2, values4, values8, values16),
        64: (values2, values4, values8, values16)
    }

    #4 padrões com 2 imagens cada
    configStripes = {
        16: (values8,),
        32: (values8, values16),
        64: (values16, values32),
        128: (values32,),
        256: (values128,),
        512: (values128,)
    }

    configChevrons = {
        512: (values8, values16, values32, values64, values128),
        1024: (values8, values16, values32, values64, values128)
    }

    #make_stripes(configStripes)
    make_boards(configBoards)


    return  images

def saveImage(image, url):
    plt.imsave(url, image, cmap='gray')
    #imsave(url, image)

def saveImages(images, url):
    for count, image in enumerate(images):
        plt.imsave(url+"/image_"+str(count)+".png", image, cmap='gray')
#        showImage(image)

def showImage(image, img_index = None, justText=False):
    if len(image.shape) >1:
        print("\n\nImage", img_index, " | Shape:", image.shape, " | Grey levels:", len(np.unique(image)))

        if not justText:
            plt.imshow(image, cmap='gray')
            plt.axis('off')
            io.imshow(image, cmap="gray")

            plt.show(block=True)